/*!                               
 *  @file      combPlots.cpp
 *  @author    Alessio Piucci
 *  @brief     Script to combine plots.
 *  @return    Returns a .root file with original and resulting plots.
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//ROOT libraries
#include <TROOT.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>

using namespace std;
namespace pt = boost::property_tree;

//function to retrieve a TH1D
TH1D get_TH1D(std::string inPath, const pt::ptree plot){
  
  std::string file_name = inPath + plot.get<std::string>("file_name");
  std::string histo_name = plot.get<std::string>("name");
  
  std::cout << "Opening file: " << file_name << std::endl;
  std::cout << "Opening histo: " << histo_name << std::endl;
  std::cout << std::endl;
  
  //open file
  TFile *inFile = TFile::Open(file_name.c_str(), "READ");
  
  if(inFile == nullptr){
    std::cout << "Error: input file " << file_name << " does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //acquire the histo
  TH1D *histo_temp = (TH1D*) inFile->Get(histo_name.c_str());
  
  if(histo_temp == NULL){
    std::cout << "Error: histo " << histo_name << " does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  TH1D histo_1D = *histo_temp;
  
  histo_1D.Sumw2();
  histo_1D.SetName(histo_name.c_str());
  histo_1D.SetTitle((plot.get<std::string>("title")).c_str());
  
  //normalize?
  if((bool) plot.get<unsigned int>("Normalize"))
    histo_1D.Scale(1./histo_1D.Integral());
  
  //close input file
  inFile->Close();
  
  //memory cleaning
  delete inFile;

  return histo_1D;
}

//function to retrieve a TH2D
TH2D get_TH2D(std::string inPath, const pt::ptree plot){
  
  std::string file_name = inPath + plot.get<std::string>("file_name");
  std::string histo_name = plot.get<std::string>("name");
  
  std::cout << "Opening file: " << file_name << std::endl;
  std::cout << "Opening histo: " << histo_name << std::endl;
  std::cout << std::endl;
  
  //open file
  TFile *inFile = TFile::Open(file_name.c_str(), "READ");
  
  if(inFile == nullptr){
    std::cout << "Error: input file " << file_name << " does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //acquire the histo
  TH2D *histo_temp = (TH2D*) inFile->Get(histo_name.c_str());
  
  if(histo_temp == NULL){
    std::cout << "Error: histo " << histo_name << " does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  TH2D histo_2D = *histo_temp;
  
  histo_2D.Sumw2();
  histo_2D.SetName(histo_name.c_str());
  histo_2D.SetTitle((plot.get<std::string>("title")).c_str());
  
  //normalize?
  if((bool) plot.get<unsigned int>("Normalize"))
    histo_2D.Scale(1./histo_2D.Integral());
  
  //close input file
  inFile->Close();
  
  //memory cleaning
  delete inFile;
  
  std::cout << "asdfasdfasdfhisto_1->GetNbinsX() = " << histo_2D.GetNbinsX() << std::endl;
  
  return histo_2D;
}

//function to combine TH1D
void comb_TH1D(std::string inPath, TFile *outFile, const pt::ptree outPlot){
    
  //----------------------------//
  //  I retrieve the operation  //
  //----------------------------//
  
  std::string operation_string = outPlot.get<std::string>("operation");
  
  bool operation_complete = false;
  
  //define the number of histos that I need for the operation
  unsigned int num_histos = 0;
  
  if(operation_string == "scale")
    num_histos = 1;
  else
    num_histos = 2;
  
  //-------------------------//
  //  I retrieve the histos  //
  //-------------------------//
    
  TH1D histo_1 = get_TH1D(inPath, outPlot.get_child("inPlots.plot1"));
  TH1D histo_2;
  
  if(num_histos > 1){
    
    histo_2 = get_TH1D(inPath, outPlot.get_child("inPlots.plot2"));
    
    //check that the number of bins and x ranges are the same for all the plots
    if(histo_1.GetNbinsX() != histo_2.GetNbinsX()){
      std::cout << "Error:: number of x-bins of plots are different." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    if((histo_1.GetXaxis()->GetXmin() != histo_2.GetXaxis()->GetXmin())
     || (histo_1.GetXaxis()->GetXmax() != histo_2.GetXaxis()->GetXmax())){
      std::cout << "Error:: x-range of plots are different." << std::endl;
      exit(EXIT_FAILURE);
    }
  }  //if(num_histos > 1)
  
  //definition of the resulting histo
  TH1D *histo_result = new TH1D("histo_result", "",
                                histo_1.GetNbinsX(),
                                histo_1.GetXaxis()->GetXmin(), histo_1.GetXaxis()->GetXmax());
  histo_result->Sumw2();
  
  //-----------------------------------------------------------//                                                                                                                                                             
  //  now I apply the required operation over the input plots  //                                                                                                                                                             
  //-----------------------------------------------------------//
  
  if(operation_string == "scale"){
    *histo_result = histo_1;
    histo_result->Scale(outPlot.get<double>("inPlots.plot1.scale_factor"));
    
    operation_complete = true;
  }
  
  if(operation_string == "+"){
    *histo_result = histo_1;
    histo_result->Add(&histo_2, 1.);
    
    operation_complete = true;
  }
  
  if(operation_string == '-'){
    *histo_result = histo_1;
    histo_result->Add(&histo_2, -1.);
    
    operation_complete = true;
  }
  
  if(operation_string == '*'){
    histo_result->Multiply(&histo_1, &histo_2, 1., 1.);
    operation_complete = true;
  }
  
  if(operation_string == '/'){
    histo_result->Divide(&histo_1, &histo_2, 1., 1.);
    operation_complete = true;
  }
  
  if(!operation_complete){
    std::cout << "Error:: invalid operation "<< operation_string << " ." << std::endl;
    std::cout << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //some style setting
  histo_result->SetTitle((outPlot.get<std::string>("title")).c_str());
  histo_result->GetXaxis()->SetTitle(histo_1.GetXaxis()->GetTitle());
  histo_result->GetYaxis()->SetTitle(histo_1.GetYaxis()->GetTitle());
  
  //writing the input and resulting histos in the output file
  outFile->cd();
  
  histo_1.Write(histo_1.GetName());
  
  if(num_histos > 1)
    histo_2.Write(histo_2.GetName());
  
  histo_result->Write((outPlot.get<std::string>("name")).c_str());
  
  //memory cleaning
  delete histo_result;
  
  return; 
}

//function to combine TH2D
void comb_TH2D(std::string inPath, TFile *outFile, const pt::ptree outPlot){
  
  //----------------------------//
  //  I retrieve the operation  //
  //----------------------------//
  
  std::string operation_string = outPlot.get<std::string>("operation");
  
  bool operation_complete = false;
  
  //define the number of histos that I need for the operation
  unsigned int num_histos = 0;
  
  if(operation_string == "scale")
    num_histos = 1;
  else
    num_histos = 2;
  
  //-------------------------//
  //  I retrieve the histos  //
  //-------------------------//
  
  TH2D histo_1 = get_TH2D(inPath, outPlot.get_child("inPlots.plot1"));
  TH2D histo_2;

  if(num_histos > 1){
    
    histo_2 = get_TH2D(inPath, outPlot.get_child("inPlots.plot2"));
    
    //check that the number of bins and x ranges are the same for all the plots
    if(histo_1.GetNbinsX() != histo_2.GetNbinsX()){
      std::cout << "Error:: number of x-bins of plots are different." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    if(histo_1.GetNbinsY() != histo_2.GetNbinsY()){
      std::cout << "Error:: number of y-bins of plots are different." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    if((histo_1.GetXaxis()->GetXmin() != histo_2.GetXaxis()->GetXmin())
       || (histo_1.GetXaxis()->GetXmax() != histo_2.GetXaxis()->GetXmax())){
      std::cout << "Error:: x-range of plots are different." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    if((histo_1.GetYaxis()->GetXmin() != histo_2.GetYaxis()->GetXmin())
       || (histo_1.GetYaxis()->GetXmax() != histo_2.GetYaxis()->GetXmax())){
      std::cout << "Error:: y-range of plots are different." << std::endl;
      exit(EXIT_FAILURE);
    }
  }  //if(num_histos > 1)
  
  //definition of resulting histo
  TH2D *histo_result = new TH2D("histo_result", "",
                                histo_1.GetNbinsX(),
                                histo_1.GetXaxis()->GetXmin(), histo_1.GetXaxis()->GetXmax(),
                                histo_1.GetNbinsY(),
                                histo_1.GetYaxis()->GetXmin(), histo_1.GetYaxis()->GetXmax());
  histo_result->Sumw2();
  
  //-----------------------------------------------------------//
  //  now I apply the required operation over the input plots  //
  //-----------------------------------------------------------//
  
  if(operation_string == "scale"){
    *histo_result = histo_1;
    histo_result->Scale(outPlot.get<double>("inPlots.plot1.scale_factor"));
    
    operation_complete = true; 
  }
  
  if(operation_string == "+"){
    *histo_result = histo_1;
    histo_result->Add(&histo_2, 1.);
    
    operation_complete = true;
  }
  
  if(operation_string == '-'){
    *histo_result = histo_1;
    histo_result->Add(&histo_2, -1.);
    
    operation_complete = true;
  }
   
  if(operation_string == '*'){
    histo_result->Multiply(&histo_1, &histo_2, 1., 1.);
    operation_complete = true;
  }
  
  if(operation_string == '/'){
    histo_result->Divide(&histo_1, &histo_2, 1., 1.);
    operation_complete = true;
  }
  
  if(!operation_complete){
    std::cout << "Error:: invalid operation." << std::endl;
    std::cout << std::endl;
    exit(EXIT_FAILURE); 
  }
  
  //some style setting
  histo_result->SetTitle((outPlot.get<std::string>("title")).c_str());
  histo_result->GetXaxis()->SetTitle(histo_1.GetXaxis()->GetTitle());
  histo_result->GetYaxis()->SetTitle(histo_1.GetYaxis()->GetTitle());
  
  //writing the input and resulting histos in the output file
  outFile->cd();
  
  histo_1.Write(histo_1.GetName());
  
  if(num_histos > 1)
    histo_2.Write(histo_2.GetName());
  
  histo_result->Write((outPlot.get<std::string>("name")).c_str());
  
  //memory cleaning
  delete histo_result;
  
  return;
}


int main(int argc, char** argv){
  
  //-----------------------//
  //  parsing job options  //
  //-----------------------//
  
  std::string configFileName = "";
  std::string inPath = "";
  TString outFileName = "";
  
  extern char* optarg;
  
  int ca;
  
  //parsing the input options
  while ((ca = getopt(argc, argv, "c:i:o:h")) != -1){  
    switch (ca){
      
    case 'c':
      configFileName = optarg;
      break;
      
    case 'i':
      inPath = optarg;
      break;
      
    case 'o':
      outFileName = optarg;
      break;

    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-c : configuration file name." << std::endl;
      std::cout << "-i : input path." << std::endl;
      std::cout << "-o : output file name." << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;
      
    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
      
    }  //switch (ca)
  }  //while ((ca = getopt(argc, argv, "i:o")) != -1)
  
  //printing of acquired options
  std::cout << "configFileName = " << configFileName << std::endl;
  std::cout << "inPath = " << inPath << std::endl;
  std::cout << "outFileName = " << outFileName << std::endl;
  
  //check of the acquired options
  if((inPath == "") || (outFileName == "") || (configFileName == "")){
    std::cout << "Error: config file name or input/output paths not correctly set." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //opening output file
  TFile* outFile = new TFile(outFileName,"RECREATE");
  
  //----------------------------------//
  //  reading the configuration file  //
  //----------------------------------//
  
  //create empty property tree object
  pt::ptree configtree;
  
  //parse the INFO into the property tree.
  pt::read_info(configFileName, configtree);
  
  //--------------------------//
  //  loop over data to plot  //
  //--------------------------//
  
  //loop over resulting plots
  for(pt::ptree::const_iterator outPlot = configtree.get_child("outPlots").begin();
      outPlot != configtree.get_child("outPlots").end(); ++outPlot){
    
    //now I have to select TH1D or TH2D
    if(outPlot->second.get<std::string>("histo") == "TH1D")
      comb_TH1D(inPath, outFile, outPlot->second);
    
    if(outPlot->second.get<std::string>("histo") == "TH2D")
      comb_TH2D(inPath, outFile, outPlot->second);
  }  //loop over resulting plots
  
  //write and close the output file
  outFile->Write();
  outFile->Close();
  
  return 0;
  
}
