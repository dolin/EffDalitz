/*!
 *  @file      yield.cpp
 *  @author    Alessio Piucci
 *  @brief     Macro to compute the yield corrected by the s-weights and the event-efficiencies.
 *  @return    Returns the value of the corrected yield and a tree for monitoring purposes.
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//ROOT libraries
#include "TROOT.h"
#include <TFile.h>
#include "TTree.h"

//ROOFIT
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooWorkspace.h"
#include "RooStats/SPlot.h"

//other libraries
#include "../include/effLUT.cpp"

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string/replace.hpp>

using namespace std;
namespace pt = boost::property_tree;

//structure to store the kinematic variables of the particle
struct PID_Kin_variables{
  std::string part_name;
  double E;
  double px;
  double py;
  double pz; 
};


int main(int argc, char** argv){
  
  
  //-----------------------//
  //  parsing job options  //
  //-----------------------//
  
  std::string inFileName = "";
  std::string inTreeName = "";
  std::string outFileName = "";
  std::string effLUT_configFileName = "";
  
  extern char* optarg;

  int ca;
  
  //parsing the input options
  while ((ca = getopt(argc, argv, "i:t:c:o:h")) != -1){
    switch (ca){

    case 'i':
      inFileName = optarg;
      break;
      
    case 't':
      inTreeName = optarg;
      break;
      
    case 'o':
      outFileName = optarg;
      break;
    
    case 'c':
      effLUT_configFileName = optarg;
      break;
      
    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-i : input file name containing data (.root file)." << std::endl;
      std::cout << "-t : input tree name." << std::endl;
      std::cout << "-o : output file name." << std::endl;
      std::cout << "-c : configuration file name." << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;

    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
      
    }  //switch (ca)
  }  //while ((ca = getopt(argc, argv, "i:c:o")) != -1)
  
  
  //print the acquired options
  std::cout << "inFileName = " << inFileName << std::endl;
  std::cout << "inTreeName = " << inTreeName << std::endl;
  std::cout << "outFileName = " << outFileName << std::endl;
  std::cout << "effLUT_configFileName = " << effLUT_configFileName << std::endl;
  
  //check of the acquired options
  if((inFileName == "") || (inTreeName == "")
     || (outFileName == "") || (effLUT_configFileName == "")){
    std::cout << "Error: configuration or input/output file names not correctly set." << std::endl;
    exit(EXIT_FAILURE); 
  }
  
  //output tree
  TFile *outFile_tree = new TFile((outFileName + "_tree.root").c_str(), "recreate");
  
  
  //-------------------------------//
  //  read the configuration file  //
  //-------------------------------//
  
  //read the configuration file with the effLUT options
  pt::ptree configtree_effLUT;
  pt::read_info(effLUT_configFileName, configtree_effLUT);
  
  //should I set dummy efficiencies?
  bool dummy_eff = false;

  if(configtree_effLUT.get_child_optional("dummy_eff.status"))
    dummy_eff = (bool) configtree_effLUT.get<unsigned int>("dummy_eff.status");
  
  effLUT *effLookUpTable = NULL;
  
  //some variables used in case of dummy efficiencies
  double kineff_dummy = 1.;
  double BDTeff_dummy = 1.;
  double PIDeff_dummy = 1.;
  
  double totaleff_dummy = 1.;
  double totaleff_err_dummy = 0.;
  
  //dummy efficiencies?
  //If not load the effLUT config file, otherwise set them
  if(!dummy_eff){
    
    //create an effLUT object
    effLookUpTable = new effLUT(effLUT_configFileName, outFile_tree);
    
    if(effLookUpTable == NULL){
      std::cout << "Error: effLUT object not created." << std::endl;
      exit(EXIT_FAILURE);
    }
  }
  else
  {
    //load dummy efficiencies
    kineff_dummy = configtree_effLUT.get<double>("dummy_eff.dummy_kin");
    BDTeff_dummy = configtree_effLUT.get<double>("dummy_eff.dummy_BDT");
    PIDeff_dummy = configtree_effLUT.get<double>("dummy_eff.dummy_PID");
  
    totaleff_dummy = kineff_dummy * BDTeff_dummy * PIDeff_dummy;
    totaleff_err_dummy = 0.;
  }
  
  
  //---------------------------------------------//
  //  read the fit results from the info files,  //
  //  later used to compute the errors           //
  //---------------------------------------------//
  
  std::string inFitInfo_fullFit_Name = "";
  std::string inFitInfo_fixedShapes_Name = "";
  std::string inFitInfo_fixedYields_Name = "";
  
  if(configtree_effLUT.get_optional<std::string>("inFitInfo_fullFit"))
    inFitInfo_fullFit_Name = ParseEnvName(configtree_effLUT.get<std::string>("inFitInfo_fullFit"));
  
  if(configtree_effLUT.get_optional<std::string>("inFitInfo_fixedShapes"))
    inFitInfo_fixedShapes_Name = ParseEnvName(configtree_effLUT.get<std::string>("inFitInfo_fixedShapes"));
  
  if(configtree_effLUT.get_optional<std::string>("inFitInfo_fixedYields"))
    inFitInfo_fixedYields_Name = ParseEnvName(configtree_effLUT.get<std::string>("inFitInfo_fixedYields"));
  
  std::cout << "inFitInfo_fullFit_Name =  " << inFitInfo_fullFit_Name << std::endl;
  std::cout << "inFitInfo_fixedShapes_Name = " << inFitInfo_fixedShapes_Name << std::endl;
  std::cout << "inFitInfo_fixedYields_Name = " << inFitInfo_fixedYields_Name << std::endl;
  std::cout << std::endl;
  
  
  //--------------------------------------//
  //  read the input data with s-weights  //
  //--------------------------------------//
  
  //open the input file and tree
  TFile* inFile = new TFile(inFileName.c_str(), "READ");
  
  if(inFile == NULL){
    std::cout << "Error: the input file does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  TTree* inTree = (TTree*) inFile->Get(inTreeName.c_str());
  
  if(inTree == NULL){
    std::cout << "Error: the input tree does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //--------------------------//
  //  set the input variables //
  //--------------------------//
  
  //s-weight of the candidate
  double s_weight;
  
  //kinematic variables of the candidate
  double Kin_1, Kin_2;
  
  //BDT variables
  std::map<std::string, std::pair<double, double>> BDT_vars;
  
  double year;
  
  //PID variables
  std::vector<PID_Kin_variables> PID_Kinvars(configtree_effLUT.get_child("PIDEff.PIDCuts").size(),
                                             PID_Kin_variables{"", 0., 0., 0., 0.});
  
  //<particle name, qvect>
  std::map<std::string, TLorentzVector> PID_qvects;

  double num_tracks_PID;

  //set the binning axis
  bool PID_varxP_varyEta = true;
  
  //variables to set on or off the BDT and PID efficiencies
  bool BDT_eff_on = false;
  bool PID_eff_on = false;

  if(configtree_effLUT.get_child_optional("BDTEff.BDTEff"))
    BDT_eff_on = (bool) configtree_effLUT.get<unsigned int>("BDTEff.BDTEff");
  
  if(configtree_effLUT.get_child_optional("PIDEff.PIDEff"))
    PID_eff_on = (bool) configtree_effLUT.get<unsigned int>("PIDEff.PIDEff");
  
  //-----------------------------------------//
  //  set the variables from the input tree  //
  //-----------------------------------------//
  
  inTree->SetBranchAddress((configtree_effLUT.get<std::string>("sweights_name")).c_str(), &s_weight);
  
  inTree->SetBranchAddress((configtree_effLUT.get<std::string>("kinEff.Kin_1_name")).c_str(), &Kin_1);
  inTree->SetBranchAddress((configtree_effLUT.get<std::string>("kinEff.Kin_2_name")).c_str(), &Kin_2);
  
  inTree->SetBranchAddress("year", &year);
  
  //only set the BDT if required
  if(BDT_eff_on){
  
    //index to fill the vector of pointers
    int index_aux = 0;
    
    std::string part_name;
    std::string var1_name, var2_name;
    
    //loop over the BDT cuts
    for(pt::ptree::const_iterator BDTEff = configtree_effLUT.get_child("BDTEff.BDTCuts").begin();
        BDTEff != configtree_effLUT.get_child("BDTEff.BDTCuts").end(); ++BDTEff, ++index_aux){
      part_name = BDTEff->second.get<std::string>("part_name");
      var1_name = BDTEff->second.get<std::string>("varname_1");
      var2_name = BDTEff->second.get<std::string>("varname_2");
      
      //create a new element in the BDT_vars
      BDT_vars.insert(std::pair<std::string, std::pair<double, double>>(part_name,
                                                                        std::make_pair(0., 0.)));
      
      //set the variables
      inTree->SetBranchAddress(var1_name.c_str(), &(BDT_vars.at(part_name).first));
      inTree->SetBranchAddress(var2_name.c_str(), &(BDT_vars.at(part_name).second));
    }  //loop over the BDT cuts
  }  //if(BDT_eff_on)
  
  //only set the PID if required
  if(PID_eff_on){
    
    if(configtree_effLUT.get_optional<unsigned int>("PIDEff.varxP_varyEta"))
      if(configtree_effLUT.get<unsigned int>("PIDEff.varxP_varyEta") == 0)
        PID_varxP_varyEta = false;

    std::cout << "PID_varxP_varyEta = " << PID_varxP_varyEta << std::endl;
    
    inTree->SetBranchAddress((configtree_effLUT.get<std::string>("PIDEff.ntracks_name")).c_str(), &num_tracks_PID);

    //index to fill the vector of pointers 
    int index_aux = 0;
    
    std::string part_name;
    
    //loop over the PID particles
    for(pt::ptree::const_iterator PIDEff = configtree_effLUT.get_child("PIDEff.PIDCuts").begin();
        PIDEff != configtree_effLUT.get_child("PIDEff.PIDCuts").end(); ++PIDEff, ++index_aux){
      
      part_name = PIDEff->second.get<std::string>("part_name");
      
      //set the PID_Kinvars element
      PID_Kinvars.at(index_aux).part_name = part_name;
      inTree->SetBranchAddress((part_name + "_PE").c_str(), &((PID_Kinvars.at(index_aux)).E));
      inTree->SetBranchAddress((part_name + "_PX").c_str(), &((PID_Kinvars.at(index_aux)).px));
      inTree->SetBranchAddress((part_name + "_PY").c_str(), &((PID_Kinvars.at(index_aux)).py));
      inTree->SetBranchAddress((part_name + "_PZ").c_str(), &((PID_Kinvars.at(index_aux)).pz));
    }  //loop over the PID particles
  }  //if(PID_eff_on)
  
  
  //------------------------//
  //  monitoring variables  //
  //------------------------//
  
  //efficiency of the current event
  Efficiency event_eff;
  
  //final result, the signal yield weighted by s-weights and event efficiency, and its error
  double signal_yield = 0.;
  double signal_yield_eff_error = 0.;
  double signal_yield_eff_error_low = 0.;
  double signal_yield_eff_error_up = 0.;
  
  //single components of the signal yield error
  double signal_yield_eff_kintot_error = 0.;
  double signal_yield_eff_kintot_error_low = 0.;
  double signal_yield_eff_kintot_error_up = 0.;
  
  double signal_yield_eff_generator_error = 0.;
  double signal_yield_eff_fiducial_error = 0.;
  double signal_yield_eff_trigger_error = 0.;
  double signal_yield_eff_reco_error = 0.;
  double signal_yield_eff_selection_error = 0.;
  
  double signal_yield_eff_BDTtot_error = 0.;
  double signal_yield_eff_BDTtot_error_low = 0.;
  double signal_yield_eff_BDTtot_error_up = 0.;
  
  double signal_yield_eff_PID_error = 0.;
  double signal_yield_eff_PID_error_low = 0.;
  double signal_yield_eff_PID_error_up = 0.;
  
  //histos with efficiencies
  TH1D *h_kin_eff_tot = new TH1D("h_kin_eff_tot", "", 300., 0., 1.1);
  TH1D *h_Generator_eff = new TH1D("h_generator_eff", "", 300., 0., 1.1);
  TH1D *h_Fiducial_eff = new TH1D("h_fiducial_eff", "", 300., 0., 1.1);
  TH1D *h_TriggerToSel_eff = new TH1D("h_TriggerToSel_eff", "", 300., 0., 1.1);
  
  TH1D *h_BDT_eff_tot = new TH1D("h_BDT_eff_tot", "", 300., 0., 1.1);
  TH1D *h_BDT_eff_1 = new TH1D("h_BDT_eff_1", "", 300., 0., 1.1);
  TH1D *h_BDT_eff_2 = new TH1D("h_BDT_eff_2", "", 300., 0., 1.1);
  
  TH1D *h_PID_eff_tot = new TH1D("h_PID_eff_tot", "", 300., 0., 1.1);
  
  TH1D *h_event_eff = new TH1D("h_event_eff", "", 300., 0., 1.1);
  
  //histos with efficiencies weighted by s-weights
  TH1D *h_kin_eff_tot_sweighted = new TH1D("h_kin_eff_tot_sweighted", "", 300., 0., 1.1);
  TH1D *h_Generator_eff_sweighted = new TH1D("h_Generator_eff_sweighted", "", 300., 0., 1.1);
  TH1D *h_Fiducial_eff_sweighted = new TH1D("h_Fiducial_eff_sweighted", "", 300., 0., 1.1);
  TH1D *h_TriggerToSel_eff_sweighted = new TH1D("h_TriggerToSel_eff_sweighted", "", 300., 0., 1.1);
  
  TH1D *h_BDT_eff_tot_sweighted = new TH1D("h_BDT_eff_tot_sweighted", "", 300., 0., 1.1);
  TH1D *h_BDT_eff_1_sweighted = new TH1D("h_BDT_eff_1_sweighted", "", 300., 0., 1.1);
  TH1D *h_BDT_eff_2_sweighted = new TH1D("h_BDT_eff_2_sweighted", "", 300., 0., 1.1);
  
  TH1D *h_PID_eff_tot_sweighted = new TH1D("h_PID_eff_tot_sweighted", "", 300., 0., 1.1);
  
  TH1D *h_event_eff_sweighted = new TH1D("h_event_eff_sweighted", "", 300., 0., 1.1);
  
  //histos with efficiencies weighted by efficiency uncertainties
  TH1D *h_kin_eff_tot_errweighted = new TH1D("h_kin_eff_tot_errweighted", "", 300., 0., 1.1);
  TH1D *h_Generator_eff_errweighted = new TH1D("h_Generator_eff_errweighted", "", 300., 0., 1.1);
  TH1D *h_Fiducial_eff_errweighted = new TH1D("h_Fiducial_eff_errweighted", "", 300., 0., 1.1);
  TH1D *h_TriggerToSel_eff_errweighted = new TH1D("h_TriggerToSel_eff_errweighted", "", 300., 0., 1.1);
  
  TH1D *h_BDT_eff_tot_errweighted = new TH1D("h_BDT_eff_tot_errweighted", "", 300., 0., 1.1);
  TH1D *h_BDT_eff_1_errweighted = new TH1D("h_BDT_eff_1_errweighted", "", 300., 0., 1.1);
  TH1D *h_BDT_eff_2_errweighted = new TH1D("h_BDT_eff_2_errweighted", "", 300., 0., 1.1);
  TH1D *h_PID_eff_tot_errweighted = new TH1D("h_PID_eff_tot_errweighted", "", 300., 0., 1.1);
  
  TH1D *h_event_eff_errweighted = new TH1D("h_event_eff_errweighted", "", 300., 0., 1.1);
  
  //efficiencies
  Efficiency kin_eff_tot;
  Efficiency TriggerToSel_eff;
  
  Efficiency BDT_eff_tot;
  Efficiency BDT_eff_1;
  Efficiency BDT_eff_2;
  
  Efficiency PID_eff_tot;
  
  Efficiency generator_eff;
  Efficiency fiducial_eff;
  Efficiency trigger_eff;
  Efficiency reco_eff;
  Efficiency selection_eff;
  
  unsigned int kin_notvalid = 0;
  unsigned int BDT_notvalid = 0;
  unsigned int PID_notvalid = 0;
  
  unsigned int skipped_events = 0;
  
  bool eff_ok = true;
  
  //yield of a given event
  double curr_yield;
  
  //to compute the mean efficiency weighted by the s-weights
  double mean_TotEff_sweighted = 0.;
  double mean_TotEff_sweighted_err = 0.;
  
  double mean_PIDEff_sweighted = 0.;
  double mean_PIDEff_sweighted_err = 0.;
  
  double sum_sweights = 0.;
  double rawyield = 0.;
  double sum_sweights_squared = 0.;
  
  double weighting_error = 0.;
  double shape_error = 0.;

  //special treatment for BDTs
  double eff_BDT_fac = 0.0, d_eff_BDT_fac_low = 0.0, d_eff_BDT_fac_up = 0.0;
  
  //-----------------------//
  //  set the output tree  //
  //-----------------------//
  
  outFile_tree->cd();
  
  TTree *out_tree = new TTree("out_tree", "out_tree");
  
  //year
  out_tree->Branch("year", &year, "year/D");
  
  //kinematic values of the candidated, used to retrieve the efficiencies
  out_tree->Branch("Kin_1", &Kin_1, "Kin_1/D");
  out_tree->Branch("Kin_2", &Kin_2, "Kin_2/D");
  
  //s-weight and corrected yield
  out_tree->Branch("s_weight", &s_weight, "s_weight/D");
  out_tree->Branch("yield", &curr_yield, "yield/D");
  
  //kinematic efficiencies
  out_tree->Branch("kin_eff_tot", &kin_eff_tot.efficiency, "kin_eff_tot/D");
  out_tree->Branch("kin_eff_tot_err", &kin_eff_tot.eff_err, "kin_eff_tot_Err/D");
  out_tree->Branch("Generator_eff", &generator_eff.efficiency, "generator_eff/D");
  out_tree->Branch("Generator_eff_err", &generator_eff.eff_err, "generator_eff_err/D");
  out_tree->Branch("Fiducial_eff", &fiducial_eff.efficiency, "fiducial_eff/D");
  out_tree->Branch("Fiducial_eff_err", &fiducial_eff.eff_err, "fiducial_eff_err/D");
  out_tree->Branch("TriggerToSel_eff", &TriggerToSel_eff.efficiency, "TriggerToSel_eff/D");
  out_tree->Branch("TriggerToSel_err_err", &TriggerToSel_eff.eff_err, "TriggerToSel_err/D");
  
  //BDT efficiencies
  out_tree->Branch("BDT_eff_tot", &BDT_eff_tot.efficiency, "BDT_eff_tot/D");
  out_tree->Branch("BDT_eff_tot_err", &BDT_eff_tot.eff_err, "BDT_eff_tot_err/D");
  out_tree->Branch("BDT_eff_1", &BDT_eff_1.efficiency, "BDT_eff_1/D");
  out_tree->Branch("BDT_eff_1_err", &BDT_eff_1.eff_err, "BDT_eff_1_err/D");
  out_tree->Branch("BDT_eff_2", &BDT_eff_2.efficiency, "BDT_eff_2/D");
  out_tree->Branch("BDT_eff_2_err", &BDT_eff_2.eff_err, "BDT_eff_2_err/D");
  
  //PID efficiency
  out_tree->Branch("PID_eff_tot", &PID_eff_tot.efficiency, "PID_eff_tot/D");
  out_tree->Branch("PID_eff_tot_err", &PID_eff_tot.eff_err, "PID_eff_tot_err/D");
  
  //total efficiency
  out_tree->Branch("event_eff", &event_eff.efficiency, "event_eff/D");
  out_tree->Branch("event_eff_err", &event_eff.eff_err, "event_eff_err/D");
  
  //----------------------------------------------//
  //  correct data by s-weights and efficiencies  //
  //----------------------------------------------//
  
  unsigned int entries_dataset = inTree->GetEntries();
  
  unsigned int candidates = 0;
  
  //loop over candidates
  for(unsigned int i_entry = 0; i_entry < entries_dataset; ++i_entry){
    
    inTree->GetEntry(i_entry);
    
    //print the progress
    if((i_entry % 1000) == 0)
      std::cout << "i_entry = " << i_entry << " / " << entries_dataset << std::endl;
    
    
    //-------------------------------//
    //  I retrieve the efficiencies  //
    //-------------------------------//
    
    eff_ok = true;
    
    //should I use dummy efficiencies?
    if(!dummy_eff){
      
      //compute the TLorentzVectors
      if(PID_eff_on){
        
        //clean the previous map of <particle_name, TLorentzVectors>
        PID_qvects.clear();
        
        //loop over the PID particles
        for(std::vector<PID_Kin_variables>::iterator it_PID = PID_Kinvars.begin();
            it_PID != PID_Kinvars.end(); ++it_PID){
           
          TLorentzVector PID_qvect_temp;
          
          PID_qvect_temp.SetPxPyPzE((*it_PID).px, (*it_PID).py, (*it_PID).pz, (*it_PID).E);
          
          //store the current TLorentzVector
          PID_qvects.insert(std::make_pair((*it_PID).part_name, PID_qvect_temp));  
          
        }  //loop over the PID particles
      }  //if(PID_eff_on)
      
      effLookUpTable->GetEventEff(Kin_1, Kin_2,
                                  BDT_vars,
                                  PID_qvects, num_tracks_PID, PID_varxP_varyEta,
                                  fiducial_eff, generator_eff, TriggerToSel_eff,
                                  kin_eff_tot, BDT_eff_tot, PID_eff_tot,
                                  event_eff, std::to_string((int) year));
    
      //some checks of the computed efficiencies,
      //for some counters
      if(kin_eff_tot.efficiency <= 0.){
        ++kin_notvalid;
        
        eff_ok = false;
      }
      
      //retrieve the single components of the kinematic efficiency
      if(!effLookUpTable->GetKinComponents(Kin_1, Kin_2,
                                           fiducial_eff, generator_eff, TriggerToSel_eff,
                                           trigger_eff, reco_eff, selection_eff, std::to_string((int) year))){
        
        eff_ok = false; 
      }

      if(BDT_eff_tot.efficiency <= 0.){
        ++BDT_notvalid;
        
        eff_ok = false;
      }
      
      if(PID_eff_tot.efficiency <= 0.){
        ++PID_notvalid;
        
        eff_ok = false;
      }
      
    }  //if(!dummy_eff)
    else
    {
      //dummy efficiencies
      kin_eff_tot.efficiency = kineff_dummy;
      BDT_eff_tot.efficiency = BDTeff_dummy;
      PID_eff_tot.efficiency = PIDeff_dummy;
      event_eff.efficiency = totaleff_dummy;
      event_eff.eff_err = totaleff_err_dummy;
    }  //if(!dummy_eff)
    
    
    //--------------------------------------------//
    //  finally, I can compute the signal yield!  //
    //--------------------------------------------//
    
    //check if the something gone wrong, with current weighted signal nan
    if((!eff_ok) || std::isnan( s_weight / event_eff.efficiency )
       || std::isinf( s_weight / event_eff.efficiency )){
      if(skipped_events < 10){
        std::cout << "Warning: an event is nan! s_weight = " << s_weight
                  << ", Kin_1 = " << Kin_1 << ", Kin_2 = " << Kin_2
                  << ". event_eff = " << event_eff.efficiency
                  << ". Generator_eff = " << generator_eff.efficiency
                  << " (passed = " << generator_eff.num_passed << ", total = " << generator_eff.num_total << ")"
                  << ", Fiducial_eff = " << fiducial_eff.efficiency
                  << " (passed = " << fiducial_eff.num_passed << ", total = " << fiducial_eff.num_total << ")"
                  << ", TriggerToSel_eff = " << TriggerToSel_eff.efficiency
                  << " (passed = " << TriggerToSel_eff.num_passed << ", total = " << TriggerToSel_eff.num_total << ")"
                  << ". trigger_eff = " << trigger_eff.efficiency
                  << " (passed = " << trigger_eff.num_passed << ", total = " << trigger_eff.num_total << ")"
                  << ", reco_eff = " << reco_eff.efficiency
                  << " (passed = " << reco_eff.num_passed << ", total = " << reco_eff.num_total << ")"
                  << ", selection_eff = " << selection_eff.efficiency
                  << " (passed = " << selection_eff.num_passed << ", total = " << selection_eff.num_total << ")"
                  << ". BDT_eff_tot = " << BDT_eff_tot.efficiency
                  << ", PID_eff_tot = " << PID_eff_tot.efficiency
                  << ". i_entry = " << i_entry << std::endl;
        std::cout << "---------------------------" << std::endl;
      }  //if(skipped_events < 10)
      
      ++skipped_events;
      
      continue;
    }
    
    //ATTENTION:
    //Doing a event by event efficiency correction using phase space integrated sweights can result in negative signal yields!
    //The reason is the following:
    //  If there is a correlation between the variables of the look up tables
    //  and the discriminating variable of the sweights, such that backgound events have smaller
    //  efficiencies, the corrected yield of a background event will on average be higher than that of a signal event.
    //  As a result, the summed absolute corrected yield of backgound-like events,
    //  whose sweights are on average negative, can be bigger than the corrected yield of signal-like events.

    curr_yield = s_weight / event_eff.efficiency;
    
    //total yield and error from the efficiency
    signal_yield += curr_yield;
    signal_yield_eff_error += pow((s_weight / pow(event_eff.efficiency, 2.)) * event_eff.eff_err, 2.);
    signal_yield_eff_error_low += pow((s_weight / pow(event_eff.efficiency, 2.)) * event_eff.eff_err_low, 2.);
    signal_yield_eff_error_up += pow((s_weight / pow(event_eff.efficiency, 2.)) * event_eff.eff_err_up, 2.);
    
    //raw yield
    rawyield += s_weight;
    
    //the same for the individual components of the efficiency
    //if E_tot = E_1 * E_2 * ... * E_n, the error contribution due to E_1 is:
    // | d(s-weight/E_tot) / d(E_1) |^2 * delta(E_1)^2 =
    // = | s-weight / (E_1^2 * (E_2 * ... * E_n)) |^2 * delta(E_1)^2
    //then I could further simplify the denominator, writing:
    // = | s-weight / (E_1 * E_tot) |^2 * delta(E_1)^2
    
    //error on the yield coming from the total kinematic efficiency
    signal_yield_eff_kintot_error += pow((s_weight / (kin_eff_tot.efficiency * event_eff.efficiency))
                                         * kin_eff_tot.eff_err, 2.);
    
    signal_yield_eff_kintot_error_low += pow((s_weight / (kin_eff_tot.efficiency * event_eff.efficiency))
                                             * kin_eff_tot.eff_err_low, 2.);
    
    signal_yield_eff_kintot_error_up += pow((s_weight / (kin_eff_tot.efficiency * event_eff.efficiency))
                                            * kin_eff_tot.eff_err_up, 2.);
    
    //error on the yield coming from the single components of the kinematic efficiency
    signal_yield_eff_generator_error += pow((s_weight / (generator_eff.efficiency * event_eff.efficiency))
                                            * generator_eff.eff_err, 2.);

    signal_yield_eff_fiducial_error += pow((s_weight / (fiducial_eff.efficiency * event_eff.efficiency))
                                           * fiducial_eff.eff_err, 2.);
    
    signal_yield_eff_trigger_error += pow((s_weight / (trigger_eff.efficiency * event_eff.efficiency))
                                          * trigger_eff.eff_err, 2.);
    

    signal_yield_eff_reco_error += pow((s_weight / (reco_eff.efficiency * event_eff.efficiency))
                                       * reco_eff.eff_err, 2.);
    

    signal_yield_eff_selection_error += pow((s_weight / (selection_eff.efficiency * event_eff.efficiency))
                                            * selection_eff.eff_err, 2.);
    
    //error on the yield coming from the BDT efficiency
    signal_yield_eff_BDTtot_error += pow((s_weight / (BDT_eff_tot.efficiency * event_eff.efficiency))
                                         * BDT_eff_tot.eff_err, 2.);
    
    signal_yield_eff_BDTtot_error_low += pow((s_weight / (BDT_eff_tot.efficiency * event_eff.efficiency))
                                             * BDT_eff_tot.eff_err_low, 2.);
    
    signal_yield_eff_BDTtot_error_up += pow((s_weight / (BDT_eff_tot.efficiency * event_eff.efficiency))
                                            * BDT_eff_tot.eff_err_up, 2.);
    
    //error on the yield coming from the PID efficiency
    signal_yield_eff_PID_error += pow((s_weight / (PID_eff_tot.efficiency * event_eff.efficiency))
                                      * PID_eff_tot.eff_err, 2.);
    
    signal_yield_eff_PID_error_low += pow((s_weight / (PID_eff_tot.efficiency * event_eff.efficiency))
                                          * PID_eff_tot.eff_err_low, 2.);
    
    signal_yield_eff_PID_error_up += pow((s_weight / (PID_eff_tot.efficiency * event_eff.efficiency))
                                         * PID_eff_tot.eff_err_up, 2.);

    //------------------------------------------//
    //  fill some histos with the efficiencies  //
    //------------------------------------------//
    
    h_kin_eff_tot->Fill(kin_eff_tot.efficiency);
    h_Fiducial_eff->Fill(fiducial_eff.efficiency);
    h_Generator_eff->Fill(generator_eff.efficiency);    
    h_TriggerToSel_eff->Fill(TriggerToSel_eff.efficiency);
    
    h_BDT_eff_tot->Fill(BDT_eff_tot.efficiency);
    h_BDT_eff_1->Fill(BDT_eff_1.efficiency);
    h_BDT_eff_2->Fill(BDT_eff_2.efficiency);
    
    h_PID_eff_tot->Fill(PID_eff_tot.efficiency);
    
    h_event_eff->Fill(event_eff.efficiency);
    
    //efficiencies weighted by the s-weights
    h_kin_eff_tot_sweighted->Fill(kin_eff_tot.efficiency, s_weight);
    
    h_Fiducial_eff_sweighted->Fill(fiducial_eff.efficiency, s_weight);
    h_Generator_eff_sweighted->Fill(generator_eff.efficiency, s_weight);
    h_TriggerToSel_eff_sweighted->Fill(TriggerToSel_eff.efficiency, s_weight);
    
    h_BDT_eff_tot_sweighted->Fill(BDT_eff_tot.efficiency, s_weight);
    h_BDT_eff_1_sweighted->Fill(BDT_eff_1.efficiency, s_weight);
    h_BDT_eff_2_sweighted->Fill(BDT_eff_2.efficiency, s_weight);
    
    h_PID_eff_tot_sweighted->Fill(PID_eff_tot.efficiency, s_weight);
    
    h_event_eff_sweighted->Fill(event_eff.efficiency, s_weight);

    //efficiencies weighted by the efficiency uncertainties
    h_kin_eff_tot_errweighted->Fill(kin_eff_tot.efficiency, 1./kin_eff_tot.eff_err);
    
    h_Fiducial_eff_errweighted->Fill(fiducial_eff.efficiency, 1./fiducial_eff.eff_err);
    h_Generator_eff_errweighted->Fill(generator_eff.efficiency, 1./generator_eff.eff_err);
    h_TriggerToSel_eff_errweighted->Fill(TriggerToSel_eff.efficiency, 1./TriggerToSel_eff.eff_err);
    
    h_BDT_eff_tot_errweighted->Fill(BDT_eff_tot.efficiency, 1./BDT_eff_tot.eff_err);
    h_BDT_eff_1_errweighted->Fill(BDT_eff_1.efficiency, 1./BDT_eff_1.eff_err);
    h_BDT_eff_2_errweighted->Fill(BDT_eff_2.efficiency, 1./BDT_eff_2.eff_err);
    h_PID_eff_tot_errweighted->Fill(PID_eff_tot.efficiency, 1./PID_eff_tot.eff_err);
    
    h_event_eff_errweighted->Fill(event_eff.efficiency, 1./event_eff.eff_err);
    
    //fill the output tree
    out_tree->Fill();

    eff_BDT_fac += s_weight*BDT_eff_tot.efficiency;
    d_eff_BDT_fac_low += std::pow(s_weight*BDT_eff_tot.eff_err_low,2);
    d_eff_BDT_fac_up  += std::pow(s_weight*BDT_eff_tot.eff_err_up,2);
    
    //stuff used to compute the mean efficiency weighted by the s-weights
    mean_TotEff_sweighted += (event_eff.efficiency * s_weight);
    mean_TotEff_sweighted_err += pow(s_weight * event_eff.eff_err, 2.);
    
    mean_PIDEff_sweighted += (PID_eff_tot.efficiency * s_weight);
    mean_PIDEff_sweighted_err += pow(s_weight * PID_eff_tot.eff_err, 2.);
    
    //some other variables used to compute the errors
    sum_sweights += s_weight;
    sum_sweights_squared += (s_weight*s_weight);
    
    //here the errors due to the s-weights is the most general due to generic weighting
    //I think that it's underestimated here, since there are no explicit errors assigned to the sweights
    //a solution could be the simple error propagation from s_weight formula  
    weighting_error += pow(s_weight/event_eff.efficiency, 2.);
    
    ++candidates;
    
  }  //loop over candidates
  
  //in the case of a negative signal yield (see above for an explanation), we set the signal yield to 1
  if(signal_yield < 0.f){
    std::printf("\033[0;36m%-20.20s \033[1;31m%-8.8s\033[0m %s\n","signal_yield","ERROR:",
                TString::Format("Setting signal yield %.1f to 1 to be able to carry on!",signal_yield).Data());
    signal_yield = 1.f;  
  }
  
  
  //----------------------//
  //  compute the errors  //
  //----------------------// 
  
  //normalize the mean efficiency weighted by the s-weights
  mean_TotEff_sweighted /= sum_sweights;
  
  mean_TotEff_sweighted_err = sqrt(mean_TotEff_sweighted_err);
  mean_TotEff_sweighted_err /= fabs(sum_sweights);
  
  mean_PIDEff_sweighted /= sum_sweights;
  
  mean_PIDEff_sweighted_err = sqrt(mean_PIDEff_sweighted_err);
  mean_PIDEff_sweighted_err /= fabs(sum_sweights);
  
  //----------------------------//
  
  //error got with fixed yields, and only shapes floating:
  //since the s-weighting is performed with fixed shapes and floating yields,
  //in principle we only need the error from the weighting and this other one
  //
  //assumption: the results with fixed yields and the ones with fixed shapes
  //are statistically independent
  
  double fixedYieldsFit_yield = 0.;
  double fixedYieldsFit_yield_error = 0.;
  
  //some other cuts used for crosschecks
  double fullFit_yield = 0.;
  double fullFit_yield_error = 0.;
  double fixedShapes_yield = 0.;
  double fixedShapes_yield_error = 0.;
  
  std::string candidate_name = "";
  
  //read a fit info file and get the error rescaled to the efficiency-corrected yield
  auto GetError_fromFit = [&signal_yield] (std::string inFitInfo_Name,
                                           double &yield, double &yield_error,
                                           const pt::ptree configtree_effLUT,
                                           std::string candidate_name){
    
    if(inFitInfo_Name == ""){
      std::cout << "Empty inFitInfo_Name, pass it." << std::endl;
      return;
    }
    
    //get the candidate name, if it was not set yet
    if(candidate_name == "")
      candidate_name = configtree_effLUT.get<std::string>("candidate_name");
    
    std::cout << "Reading " << inFitInfo_Name << std::endl;
    std::cout << "candidate_name = " << candidate_name << std::endl;
    
    //open the fit info file
    pt::ptree tree_FitInfo;
    pt::read_info(inFitInfo_Name, tree_FitInfo);
    
    yield = tree_FitInfo.get<double>(candidate_name + ".Value");
    yield_error = tree_FitInfo.get<double>(candidate_name + ".Error");
    
  };
  
  //get the error fixing the yields (just for crosscheck)
  GetError_fromFit(inFitInfo_fixedYields_Name,
                   fixedYieldsFit_yield, fixedYieldsFit_yield_error,
                   configtree_effLUT, candidate_name);
  
  //get the error fixing the shapes
  GetError_fromFit(inFitInfo_fixedShapes_Name,
                   fixedShapes_yield, fixedShapes_yield_error,
                   configtree_effLUT, candidate_name);
  
  //get the error from the full fit, floating both yield and shapes
  GetError_fromFit(inFitInfo_fullFit_Name,
                   fullFit_yield, fullFit_yield_error,
                   configtree_effLUT, candidate_name);
  
  //print the errors from the fits
  std::cout << std::endl;
  std::cout << "Fixing yields (for crosscheck) = "
            << fixedYieldsFit_yield << " +- " << fixedYieldsFit_yield_error << std::endl;
  std::cout << "Fixing shapes = "
            << fixedShapes_yield << " +- " << fixedShapes_yield_error << std::endl;
  std::cout << "Floating shapes and yield = "
            << fullFit_yield << " +- " << fullFit_yield_error << std::endl;
  std::cout << std::endl;
  
  //----------------------//

  //statistical error from the weights
  weighting_error = sqrt(weighting_error);
  
  shape_error = sqrt(pow(fullFit_yield_error, 2.) - pow(fixedShapes_yield_error, 2.));

  if(std::isnan(shape_error) || (shape_error < 0.)){
    std::cout << "Warning: the shape error is unvalid." << std::endl;
    std::cout << "--> fullFit_yield_error = " << fullFit_yield_error << std::endl;
    std::cout << "--> fixedShapes_yield_error = " << fixedShapes_yield_error << std::endl;
    std::cout << "-----> shape_error = " << shape_error << std::endl;
    shape_error = 0.;
  }
  
  //root of the signal_yield_eff_error
  signal_yield_eff_error = sqrt(signal_yield_eff_error);
  signal_yield_eff_error_low = sqrt(signal_yield_eff_error_low);
  signal_yield_eff_error_up = sqrt(signal_yield_eff_error_up);
  
  //compute the final efficiencies and errors
  signal_yield_eff_kintot_error = sqrt(signal_yield_eff_kintot_error);
  signal_yield_eff_kintot_error_low = sqrt(signal_yield_eff_kintot_error_low);
  signal_yield_eff_kintot_error_up = sqrt(signal_yield_eff_kintot_error_up);
  
  signal_yield_eff_generator_error = sqrt(signal_yield_eff_generator_error);
  signal_yield_eff_fiducial_error = sqrt(signal_yield_eff_fiducial_error);
  signal_yield_eff_trigger_error = sqrt(signal_yield_eff_trigger_error);
  signal_yield_eff_reco_error = sqrt(signal_yield_eff_reco_error);
  signal_yield_eff_selection_error = sqrt(signal_yield_eff_selection_error);
  
  signal_yield_eff_BDTtot_error = sqrt(signal_yield_eff_BDTtot_error);
  signal_yield_eff_BDTtot_error_low = sqrt(signal_yield_eff_BDTtot_error_low);
  signal_yield_eff_BDTtot_error_up = sqrt(signal_yield_eff_BDTtot_error_up);
  
  signal_yield_eff_PID_error = sqrt(signal_yield_eff_PID_error);
  signal_yield_eff_PID_error_low = sqrt(signal_yield_eff_PID_error_low);
  signal_yield_eff_PID_error_up = sqrt(signal_yield_eff_PID_error_up);
  
  //error coming from the sweighting and the fits: use the Lb --> Chic p k analysis for reference
  // https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/Lb2chicpK
  double yield_stat_error = sqrt(pow(weighting_error, 2.)
                                 + (signal_yield/candidates)*pow(shape_error, 2.));

  //print some statistics
  std::cout << std::endl;
  std::cout << "Yield = " << signal_yield
            << " +- " << yield_stat_error << "(stat) (" << (yield_stat_error/signal_yield)*100. << " %)"
            << " +- " << signal_yield_eff_error << " (eff) (" << (signal_yield_eff_error/signal_yield)*100 << " %)"
            << "(- " << signal_yield_eff_error_low << ", + " << signal_yield_eff_error_up << ")" << std::endl;
  
  std::cout << "components of the statistical uncertainty:" << std::endl;
  std::cout << "---> +- " << weighting_error << " (weighting) (" << (weighting_error/signal_yield)*100. << " %)" << std::endl;
  std::cout << "---> +- " << shape_error << " (fromShape) (" << (shape_error/signal_yield)*100. << " %)" << std::endl;
  std::cout << "components of the systematic uncertainty (efficiency):" << std::endl;
  std::cout << "---> +- " << (signal_yield_eff_kintot_error/signal_yield)*100 << " % total kin" << std::endl;
  std::cout << "------> +- " << (signal_yield_eff_generator_error/signal_yield)*100 << " % generator" << std::endl;
  std::cout << "------> +- " << (signal_yield_eff_fiducial_error/signal_yield)*100 << " % fiducial" << std::endl;
  std::cout << "------> +- " << (signal_yield_eff_trigger_error/signal_yield)*100 << " % trigger" << std::endl;
  std::cout << "------> +- " << (signal_yield_eff_reco_error/signal_yield)*100 << " % reconstruction"  << std::endl;
  std::cout << "------> +- " << (signal_yield_eff_selection_error/signal_yield)*100 << " % selection" << std::endl;
  std::cout << "---> +- " << (signal_yield_eff_BDTtot_error/signal_yield)*100 << " % BDT" << std::endl;
  std::cout << "---> +- " << (signal_yield_eff_PID_error/signal_yield)*100 << " % PID)" <<std::endl;
  std::cout << std::endl;

  std::cout << "----------------" << std::endl;
  std::cout << "--- !!! The following are quantities computed over the selected events only !!! ---" << std::endl;
  std::cout << "----------------" << std::endl;
  std::cout << std::endl;
  
  std::cout << "----------------" << std::endl;
  std::cout << "--- Simple mean of the efficiency" << std::endl;
  std::cout << std::endl;
  std::cout << "simple kin_eff_tot_mean = " << h_kin_eff_tot->GetMean() << ", stdv = " << h_kin_eff_tot->GetStdDev()
            << " (" << (h_kin_eff_tot->GetStdDev() / h_kin_eff_tot->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> Fiducial_eff_mean = " << h_Fiducial_eff->GetMean()
            << ", stdv = " << h_Fiducial_eff->GetStdDev()
            << " (" << (h_Fiducial_eff->GetStdDev() / h_Fiducial_eff->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> Generator_eff_mean = " << h_Generator_eff->GetMean()
            << ", stdv = " << h_Generator_eff->GetStdDev()
            << " (" << (h_Generator_eff->GetStdDev() / h_Generator_eff->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> TriggerToSel_eff_mean = " << h_TriggerToSel_eff->GetMean()
            << ", stdv = " << h_TriggerToSel_eff->GetStdDev()
            << " (" << (h_TriggerToSel_eff->GetStdDev() / h_TriggerToSel_eff->GetMean())*100 << "%)" << std::endl;
  std::cout << std::endl;
  
  std::cout << "simple BDT_eff_tot_mean = " << h_BDT_eff_tot->GetMean() << ", stdv = " << h_BDT_eff_tot->GetStdDev() 
            << " (" << (h_BDT_eff_tot->GetStdDev() / h_BDT_eff_tot->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> BDT_eff_1_mean = " << h_BDT_eff_1->GetMean() << ", stdv = " << h_BDT_eff_1->GetStdDev()
            << " (" << (h_BDT_eff_1->GetStdDev() / h_BDT_eff_1->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> BDT_eff_2_mean = " << h_BDT_eff_2->GetMean() << ", stdv = " << h_BDT_eff_2->GetStdDev()
            << " (" << (h_BDT_eff_2->GetStdDev() / h_BDT_eff_2->GetMean())*100 << "%)" << std::endl;
  std::cout << std::endl;
  
  std::cout << "simple PID_eff_mean = " << h_PID_eff_tot->GetMean() << ", stdv = " << h_PID_eff_tot->GetStdDev() 
            << " (" << (h_PID_eff_tot->GetStdDev() / h_PID_eff_tot->GetMean())*100 << "%)" << std::endl;
  std::cout << std::endl;
  std::cout << "simple event_eff_mean = " << h_event_eff->GetMean() << ", stdv = " << h_event_eff->GetStdDev() 
            << " (" << (h_event_eff->GetStdDev() / h_event_eff->GetMean())*100 << "%)" << std::endl;
  std::cout << std::endl;
  
  std::cout << "----------------" << std::endl;
  std::cout << "--- Weighted average of the efficiency, weighted by the sweights" << std::endl;
  std::cout << std::endl;
  std::cout << "sweighted kin_eff_tot_mean = " << h_kin_eff_tot_sweighted->GetMean()
            << ", stdv = " << h_kin_eff_tot_sweighted->GetStdDev()
            << " (" << (h_kin_eff_tot_sweighted->GetStdDev() / h_kin_eff_tot_sweighted->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> Fiducial_eff_mean = " << h_Fiducial_eff_sweighted->GetMean()
            << ", stdv = " << h_Fiducial_eff_sweighted->GetStdDev()
            << " (" << (h_Fiducial_eff_sweighted->GetStdDev() / h_Fiducial_eff_sweighted->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> Generator_eff_mean = " << h_Generator_eff_sweighted->GetMean()
            << ", stdv = " << h_Generator_eff_sweighted->GetStdDev()
            << " (" << (h_Generator_eff_sweighted->GetStdDev() / h_Generator_eff_sweighted->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> TriggerToSel_eff_mean = " << h_TriggerToSel_eff_sweighted->GetMean()
            << ", stdv = " << h_TriggerToSel_eff_sweighted->GetStdDev()
            << " (" << (h_TriggerToSel_eff_sweighted->GetStdDev() / h_TriggerToSel_eff_sweighted->GetMean())*100
            << "%)" << std::endl;
  std::cout << std::endl;
  
  std::cout << "sweighted BDT_eff_tot_mean = " << h_BDT_eff_tot_sweighted->GetMean()
            << ", stdv = " << h_BDT_eff_tot_sweighted->GetStdDev()
            << " (" << (h_BDT_eff_tot_sweighted->GetStdDev() / h_BDT_eff_tot_sweighted->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> BDT_eff_tot_mean = " << h_BDT_eff_1_sweighted->GetMean() << ", stdv = " << h_BDT_eff_1_sweighted->GetStdDev()
            << " (" << (h_BDT_eff_1_sweighted->GetStdDev() / h_BDT_eff_1_sweighted->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> BDT_eff_tot_mean = " << h_BDT_eff_2_sweighted->GetMean() << ", stdv = " << h_BDT_eff_2_sweighted->GetStdDev()
            << " (" << (h_BDT_eff_2_sweighted->GetStdDev() / h_BDT_eff_2_sweighted->GetMean())*100 << "%)" << std::endl;
  std::cout << std::endl;
  
  std::cout << "sweighted PID_eff_mean = " << h_PID_eff_tot_sweighted->GetMean()
            << ", stdv = " << h_PID_eff_tot_sweighted->GetStdDev()
            << " (" << (h_PID_eff_tot_sweighted->GetStdDev() / h_PID_eff_tot_sweighted->GetMean())*100 << "%)" << std::endl;
  std::cout << std::endl;
  std::cout << "sweighted event_eff_mean = " << h_event_eff_sweighted->GetMean()
            << ", stdv = " << h_event_eff_sweighted->GetStdDev()
            << " (" << (h_event_eff_sweighted->GetStdDev() / h_event_eff_sweighted->GetMean())*100 << "%)" << std::endl;
  std::cout << std::endl;

  std::cout << "----------------" << std::endl;
  std::cout << "mean_TotEff_sweighted = " << mean_TotEff_sweighted << " +- " << mean_TotEff_sweighted_err << std::endl;
  std::cout << "----------------" << std::endl;

  std::cout << "----------------" << std::endl;
  std::cout << "--- Weighted average of the efficiency, weighted by the eff uncertainties" << std::endl;
  std::cout << std::endl;
  std::cout << "errweighted kin_eff_tot_mean = " << h_kin_eff_tot_errweighted->GetMean()
            << ", stdv = " << h_kin_eff_tot_errweighted->GetStdDev()
            << " (" << (h_kin_eff_tot_errweighted->GetStdDev() / h_kin_eff_tot_errweighted->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> Fiducial_eff_mean = " << h_Fiducial_eff_errweighted->GetMean()
            << ", stdv = " << h_Fiducial_eff_errweighted->GetStdDev()
            << " (" << (h_Fiducial_eff_errweighted->GetStdDev() / h_Fiducial_eff_errweighted->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> Generator_eff_mean = " << h_Generator_eff_errweighted->GetMean()
            << ", stdv = " << h_Generator_eff_errweighted->GetStdDev()
            << " (" << (h_Generator_eff_errweighted->GetStdDev() / h_Generator_eff_errweighted->GetMean())*100 << "%)" << std::endl;
  std::cout << "--> TriggerToSel_eff_mean = " << h_TriggerToSel_eff_errweighted->GetMean()
            << ", stdv = " << h_TriggerToSel_eff_errweighted->GetStdDev()
            << " (" << (h_TriggerToSel_eff_errweighted->GetStdDev() / h_TriggerToSel_eff_errweighted->GetMean())*100
            << "%)" << std::endl;
  std::cout << std::endl;
  
  //print how many not valid events there were
  std::cout << "skipped kin events = " << kin_notvalid
            << " (" << (kin_notvalid / (double) entries_dataset)*100. << "%)" << std::endl;
  std::cout << "skipped BDT events = " << BDT_notvalid
            << " (" << (BDT_notvalid / (double) entries_dataset)*100. << "%)" << std::endl;
  std::cout << "skipped PID events = " << PID_notvalid
            << " (" << (PID_notvalid / (double) entries_dataset)*100. << "%)" << std::endl;
  
  //if the not-valid cases are more then 1% of entries, print a warning
  if((kin_notvalid / (double) entries_dataset) > 0.01)
    std::cout << "WARNING: too many not valid kinematic cases! Please have a look!" << std::endl;
  
  if((BDT_notvalid / (double) entries_dataset) > 0.01)
    std::cout << "WARNING: too many not valid BDT cases! Please have a look!" << std::endl;
  
  if((PID_notvalid / (double) entries_dataset) > 0.01)
    std::cout << "WARNING: too many not valid PID cases! Please have a look!" << std::endl;
  
  if((skipped_events / (double) entries_dataset) > 0.01)
    std::cout << "WARNING: too many skipped events! Please have a look!" << std::endl;
  std::cout << std::endl;
  
  
  //save the results in a output INFO file
  pt::ptree log_out;
  
  log_out.put<double>("entries_dataset", entries_dataset);
  log_out.put<double>("skipped_events", skipped_events);
  log_out.put<double>("kin_notvalid", kin_notvalid);
  log_out.put<double>("BDT_notvalid", BDT_notvalid);
  log_out.put<double>("PID_notvalid", PID_notvalid);
  
  log_out.put<double>("Signal_yield.value", signal_yield);
  log_out.put<double>("Signal_yield.Error.Statistical.value", yield_stat_error);
  
  //single contributions to the error
  log_out.put<double>("Signal_yield.Error.Shape.value", shape_error);
  log_out.put<double>("Signal_yield.Error.Weighting.value", weighting_error);
  log_out.put<double>("Signal_yield.Error.Efficiency.value", signal_yield_eff_error);
  log_out.put<double>("Signal_yield.Error.Efficiency.value_low", signal_yield_eff_error_low);
  log_out.put<double>("Signal_yield.Error.Efficiency.value_up", signal_yield_eff_error_up);
  
  log_out.put<double>("Signal_yield.Error.EfficiencyKinTot.value", signal_yield_eff_kintot_error);
  log_out.put<double>("Signal_yield.Error.EfficiencyKinTot.value_low", signal_yield_eff_kintot_error_low);
  log_out.put<double>("Signal_yield.Error.EfficiencyKinTot.value_up", signal_yield_eff_kintot_error_up);
  
  log_out.put<double>("Signal_yield.Error.EfficiencyGenerator.value", signal_yield_eff_generator_error);
  log_out.put<double>("Signal_yield.Error.EfficiencyFiducial.value", signal_yield_eff_fiducial_error);
  log_out.put<double>("Signal_yield.Error.EfficiencyTrigger.value", signal_yield_eff_trigger_error);
  log_out.put<double>("Signal_yield.Error.EfficiencyReconstruction.value", signal_yield_eff_reco_error);
  log_out.put<double>("Signal_yield.Error.EfficiencySelection.value", signal_yield_eff_selection_error);
  
  log_out.put<double>("Signal_yield.Error.EfficiencyBDTTot.value", signal_yield_eff_BDTtot_error);
  log_out.put<double>("Signal_yield.Error.EfficiencyBDTTot.value_low", signal_yield_eff_BDTtot_error_low);
  log_out.put<double>("Signal_yield.Error.EfficiencyBDTTot.value_up", signal_yield_eff_BDTtot_error_up);
  
  log_out.put<double>("Signal_yield.Error.EfficiencyPID.value", signal_yield_eff_PID_error);
  log_out.put<double>("Signal_yield.Error.EfficiencyPID.value_low", signal_yield_eff_PID_error_low);
  log_out.put<double>("Signal_yield.Error.EfficiencyPID.value_up", signal_yield_eff_PID_error_up);
  
  //summary of the simple average efficiencies
  log_out.put<double>("SimpleEff.TotalEff.value", h_event_eff->GetMean());
  log_out.put<double>("SimpleEff.TotalEff.Error.value", h_event_eff->GetStdDev());
  
  log_out.put<double>("SimpleEff.TotalKinEff.value", h_kin_eff_tot->GetMean());
  log_out.put<double>("SimpleEff.TotalKinEff.Error.value", h_kin_eff_tot->GetStdDev());
  log_out.put<double>("SimpleEff.FiducialEff.value", h_Fiducial_eff->GetMean());
  log_out.put<double>("SimpleEff.FiducialEff.Error.value", h_Fiducial_eff->GetStdDev());
  log_out.put<double>("SimpleEff.GeneratorEff.value", h_Generator_eff->GetMean());
  log_out.put<double>("SimpleEff.GeneratorEff.Error.value", h_Generator_eff->GetStdDev());
  
  log_out.put<double>("SimpleEff.BDTTotEff.value", h_BDT_eff_tot->GetMean());
  log_out.put<double>("SimpleEff.BDTTotEff.Error.value", h_BDT_eff_tot->GetStdDev());
  
  log_out.put<double>("SimpleEff.PIDTotEff.value", h_PID_eff_tot->GetMean());
  log_out.put<double>("SimpleEff.PIDTotEff.Error.value", h_PID_eff_tot->GetStdDev());

  //summary of the s-weighted average efficiencies
  log_out.put<double>("SWeightedEff.TotalEff.value", h_event_eff_sweighted->GetMean());
  log_out.put<double>("SWeightedEff.TotalEff.Error.value", h_event_eff_sweighted->GetStdDev());
  
  log_out.put<double>("SWeightedEff.TotalKinEff.value", h_kin_eff_tot_sweighted->GetMean());
  log_out.put<double>("SWeightedEff.TotalKinEff.Error.value", h_kin_eff_tot_sweighted->GetStdDev());
  log_out.put<double>("SWeightedEff.FiducialEff.value", h_Fiducial_eff_sweighted->GetMean());
  log_out.put<double>("SWeightedEff.FiducialEff.Error.value", h_Fiducial_eff_sweighted->GetStdDev());
  log_out.put<double>("SWeightedEff.GeneratorEff.value", h_Generator_eff_sweighted->GetMean());
  log_out.put<double>("SWeightedEff.GeneratorEff.Error.value", h_Generator_eff_sweighted->GetStdDev());
  
  log_out.put<double>("SWeightedEff.BDTTotEff.value", h_BDT_eff_tot_sweighted->GetMean());
  log_out.put<double>("SWeightedEff.BDTTotEff.Error.value", h_BDT_eff_tot_sweighted->GetStdDev());
    
  log_out.put<double>("SWeightedEff.PIDTotEff.value", h_PID_eff_tot_sweighted->GetMean());
  log_out.put<double>("SWeightedEff.PIDTotEff.Error.value", h_PID_eff_tot_sweighted->GetStdDev());

  //summary of the err-weighted average efficiencies
  log_out.put<double>("ErrWeightedEff.TotalEff.value", h_event_eff_errweighted->GetMean());
  log_out.put<double>("ErrWeightedEff.TotalEff.Error.value", h_event_eff_errweighted->GetStdDev());
  
  log_out.put<double>("ErrWeightedEff.TotalKinEff.value", h_kin_eff_tot_errweighted->GetMean());
  log_out.put<double>("ErrWeightedEff.TotalKinEff.Error.value", h_kin_eff_tot_errweighted->GetStdDev());
  log_out.put<double>("ErrWeightedEff.FiducialEff.value", h_Fiducial_eff_errweighted->GetMean());
  log_out.put<double>("ErrWeightedEff.FiducialEff.Error.value", h_Fiducial_eff_errweighted->GetStdDev());
  log_out.put<double>("ErrWeightedEff.GeneratorEff.value", h_Generator_eff_errweighted->GetMean());
  log_out.put<double>("ErrWeightedEff.GeneratorEff.Error.value", h_Generator_eff_errweighted->GetStdDev());
  
  log_out.put<double>("ErrWeightedEff.BDTTotEff.value", h_BDT_eff_tot_errweighted->GetMean());
  log_out.put<double>("ErrWeightedEff.BDTTotEff.Error.value", h_BDT_eff_tot_errweighted->GetStdDev());
    
  log_out.put<double>("ErrWeightedEff.PIDTotEff.value", h_PID_eff_tot_errweighted->GetMean());
  log_out.put<double>("ErrWeightedEff.PIDTotEff.Error.value", h_PID_eff_tot_errweighted->GetStdDev());
  
  /////
  
  log_out.put<double>("BDTEff_fac",eff_BDT_fac/fabs(sum_sweights));
  log_out.put<double>("BDTdEff_fac_low",std::sqrt(d_eff_BDT_fac_low)/fabs(sum_sweights));
  log_out.put<double>("BDTdEff_fac_up",std::sqrt(d_eff_BDT_fac_up)/fabs(sum_sweights));
  
  //write the output .info file
  write_info(outFileName + "_log.info", log_out);
  
  //write the output file
  outFile_tree->cd();
  out_tree->Write();
  h_kin_eff_tot->Write();
  outFile_tree->Write();
  outFile_tree->Close();
    
  return 0;
}
  
  
