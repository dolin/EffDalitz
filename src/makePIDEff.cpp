/*!
 *  @file      makePIDEff.cpp
 *  @author    Alessio Piucci
 *  @date      12-09-2016
 *  @brief     Script to make efficiency plots for the PID cuts
 *  @return    Create an output root file containing the efficiency plots
 */

//from std
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <unistd.h>  //to use getopt in the parser
#include <tuple>
#include <fstream>   //to write the log in an external stream

#include "../include/commonLib.h"
#include "../2DAdaptiveBinning/include/TH2A.h"

//from ROOT
#include <TROOT.h>
#include "TFile.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TVectorD.h"

using namespace std;

//To create efficiency plots with TEfficiency
void makeTEfficiency(TH3D *h_PID_passed, TH3D *h_PID_total,
                     TH3D *h_PID_errors_pass, TH3D *h_PID_errors_total,
                     TFile *outFile, std::string outHisto_name, unsigned int switch_axis){
  
  //IMPLEMENT ONLY IF NEEDED
  if(switch_axis){
    std::cout << "Error: you are trying to reverse the xy axis with TEfficiency. Functionality not yet implemented." << std::endl;
    exit(EXIT_FAILURE);
  }  
  
  //the TEfficiency doesn't like not empty underflow and overflow bins
  h_PID_total->ClearUnderflowAndOverflow();
  h_PID_passed->ClearUnderflowAndOverflow();
  
  //counter used for debug
  unsigned int num_fixed_bins = 0;
  
  //check consistency of generated and reco histograms
  if(!TEfficiency::CheckConsistency(*h_PID_passed, *h_PID_total)){
    std::cout << "Warning: passed and total histos are not consistent! Trying to fix them." << std::endl;
    
    FixTEfficiencyEntries(h_PID_passed, h_PID_total, h_PID_errors_pass, h_PID_errors_total, num_fixed_bins);
    //exit(EXIT_FAILURE);
  }
  
  std::cout << "num_fixed_bins for TEfficiency = " << num_fixed_bins
            << " (" << (num_fixed_bins/(double) h_PID_passed->GetNcells())*100 << "%)" << std::endl;
  
  TEfficiency *h_PID_eff = new TEfficiency(*h_PID_passed, *h_PID_total);
  
  h_PID_eff->SetName((outHisto_name + "_eff").c_str());
  
  //use Wilson statistic, please don't ask me but use this reference:
  //https://root.cern.ch/doc/master/classTEfficiency.html
  h_PID_eff->SetConfidenceLevel(0.683);
  h_PID_eff->SetStatisticOption(TEfficiency::kFWilson);
  
  //write the out histos
  outFile->cd();
  
  h_PID_eff->Write(h_PID_eff->GetName());
  h_PID_errors_pass->Write(h_PID_errors_pass->GetName());
  h_PID_errors_total->Write(h_PID_errors_total->GetName());
  
  return;
  
}

//To create efficiency plots with adaptive binning
void makeAdaptive(TH3D *h_PID_passed, TH3D *h_PID_total,
                  TFile *outFile, std::string outHisto_name,
                  std::ofstream &log_stream, unsigned int switch_axis,
                  std::vector<int> num_min_events){
  
  //the current adaptive binning implementation only supports 2D distributions,
  //but the PID efficiency has 3 dimensions.
  //So I create n 2D efficiency distributions, where n is the number of bins in the third PID dimensions
  //then I store the 2D distributions in a tuple, together with the min and max z range of them
  
  //vector of tuple of: min_z, max_z, 2D PID efficiency distribution
  std::vector<std::tuple<double,double,TH2A*>> PID_eff_vect;
  
  //loop over the z bins
  //it starts from i_zbin = 1 index, since the = 0 is the underflow
  for(int i_zbin = 1; i_zbin <= h_PID_total->GetNbinsZ(); ++i_zbin){
    
    //create a copy of the original histos
    TH3D *h_PID_total_copy = (TH3D*) h_PID_total->Clone();
    TH3D *h_PID_passed_copy = (TH3D*) h_PID_passed->Clone();
    
    std::cout << "bin = " << i_zbin << ", n_tracks = " << h_PID_total_copy->GetZaxis()->GetBinLowEdge(i_zbin) 
              << " - " << h_PID_total_copy->GetZaxis()->GetBinUpEdge(i_zbin) << std::endl;
    log_stream << "bin = " << i_zbin << ", n_tracks = " << h_PID_total_copy->GetZaxis()->GetBinLowEdge(i_zbin)
               << " - " << h_PID_total_copy->GetZaxis()->GetBinUpEdge(i_zbin) << std::endl;
    
    h_PID_total_copy->GetZaxis()->SetRangeUser(h_PID_total_copy->GetZaxis()->GetBinLowEdge(i_zbin),
                                               h_PID_total_copy->GetZaxis()->GetBinUpEdge(i_zbin));
    h_PID_passed_copy->GetZaxis()->SetRangeUser(h_PID_total_copy->GetZaxis()->GetBinLowEdge(i_zbin),
                                                h_PID_total_copy->GetZaxis()->GetBinUpEdge(i_zbin));
    
    //Project3D rocks!!
    TH2D *h_PID_2Dtotal;
    
    if(switch_axis == 0)
      h_PID_2Dtotal = (TH2D*) h_PID_total_copy->Project3D(("yx_tot" + std::to_string(i_zbin)).c_str());
    else
      h_PID_2Dtotal = (TH2D*) h_PID_total_copy->Project3D(("xy_tot" + std::to_string(i_zbin)).c_str());
    
    h_PID_2Dtotal->SetName((outHisto_name + "_2Dtotal_" + std::to_string(i_zbin)).c_str());
  
    TH2D *h_PID_2Dpassed;
    
    if(switch_axis == 0)
      h_PID_2Dpassed = (TH2D*) h_PID_passed_copy->Project3D(("yx_pass" + std::to_string(i_zbin)).c_str());
    else
      h_PID_2Dpassed = (TH2D*) h_PID_passed_copy->Project3D(("xy_pass" + std::to_string(i_zbin)).c_str());
    
    h_PID_2Dpassed->SetName((outHisto_name + "_2Dpassed_" + std::to_string(i_zbin)).c_str());
    
    //now fill the adaptive stuff
    TH2A h_gen_adaptive;
    TH2A h_reco_adaptive;
    
    //for debug
    //h_gen_adaptive.SetVerbosity(3);
    //h_reco_adaptive.SetVerbosity(3);
    
    //fill the adaptive binning histos
    h_gen_adaptive.MakeBinning(std::vector<TH2D>{*h_PID_2Dtotal, *h_PID_2Dpassed}, num_min_events.at(i_zbin - 1));
    h_reco_adaptive = h_gen_adaptive;
    h_reco_adaptive.FillFromHist(*h_PID_2Dpassed);
    
    TH2A *h_PID_eff = new TH2A();
    
    h_PID_eff->SetName((outHisto_name + "_eff_" + std::to_string(i_zbin)).c_str());
    h_PID_eff->SetTitle(("adaptive PID eff, nTracks = "
                         + std::to_string(h_PID_total_copy->GetZaxis()->GetBinLowEdge(i_zbin)) + " - "
                         + std::to_string(h_PID_total_copy->GetZaxis()->GetBinUpEdge(i_zbin))).c_str());
    
    unsigned int num_fixed_bins = 0;
    
    //check the consistency of the reco and gen histos
    CheckConsistency_adaptive(&h_gen_adaptive, &h_reco_adaptive, num_fixed_bins, false);
    
    std::cout << "num_bins = " << h_gen_adaptive.GetNBins() << std::endl;
    std::cout << "num_fixed_bins = " << num_fixed_bins
              << " (" << (((double) num_fixed_bins)/h_gen_adaptive.GetNBins())*100.  << "%)" << std::endl;
    
    log_stream << "num_bins = " << h_gen_adaptive.GetNBins() << std::endl;
    log_stream << "num_fixed_bins = " << num_fixed_bins
               << " (" << (((double) num_fixed_bins)/h_gen_adaptive.GetNBins())*100.  << "%)" << std::endl;
    
    //use Wilson statistic, please don't ask me but use this reference:
    //https://root.cern.ch/doc/master/classTEfficiency.html
    h_PID_eff->SetStatOpt(TEfficiency::kFWilson);
    int status_adaptive = h_PID_eff->BinomialDivide(h_reco_adaptive, h_gen_adaptive, true);
    
    std::cout << "status_adaptive = " << status_adaptive << std::endl;
    log_stream << "status_adaptive = " << status_adaptive << std::endl;
    
    //add the current histo to the tuple of histos
    //PID_eff_vect.push_back(std::make_tuple(h_PID_total_copy->GetZaxis()->GetBinLowEdge(i_zbin),
    //                                       h_PID_total_copy->GetZaxis()->GetBinUpEdge(i_zbin),
    //                                       h_PID_eff));
    
    //save the current histo and the min, max ranges to the root file
    outFile->cd();
    h_PID_eff->Write(h_PID_eff->GetName());
    
    TVectorD zRanges(2);
    zRanges[0] = h_PID_total_copy->GetZaxis()->GetBinLowEdge(i_zbin);
    zRanges[1] = h_PID_total_copy->GetZaxis()->GetBinUpEdge(i_zbin);
    
    outFile->cd();
    zRanges.Write((std::string(h_PID_eff->GetName()) + "_zRanges").c_str());
    
    //same the TH2D histos as well
    h_PID_2Dtotal->Write(h_PID_2Dtotal->GetName());
    h_PID_2Dpassed->Write(h_PID_2Dpassed->GetName());
    
    //clean the memory
    delete h_PID_total_copy;
    delete h_PID_passed_copy;
  }
  
  //write the output
  //outFile->cd();
  //gFile->WriteObject(&PID_eff_vect, (outHisto_name + "_vector").c_str());
  
  //write in the output the number Z bins, stored as a TVectorD
  TVectorD numZbins(1);
  
  numZbins[0] = h_PID_total->GetNbinsZ();
  
  outFile->cd();
  numZbins.Write((outHisto_name + "_eff_numZbins").c_str());
  
  return;
  
}

int main(int argc, char** argv){

  //-----------------------//
  //  parsing job options  //
  //-----------------------//
  
  std::string inFile_name = "";
  std::string outFile_name = "";
  std::string inHistoTotal_name = "";
  std::string inHistoPassed_name = "";
  std::string outHisto_name = "h_PID"; 
  
  unsigned int adaptive = 1;
  unsigned int switch_axis = 0;
  
  int min_events_1 = -1;
  int min_events_2 = -1;
  int min_events_3 = -1;
  int min_events_4 = -1;
  int min_events_5 = -1;
  
  extern char* optarg;
  
  int ca;
  
  //parsing the input options
  while ((ca = getopt(argc, argv, "i:o:t:p:a:r:1:2:3:4:5:h")) != -1){
    
    switch (ca){
      
    case 'i':
      inFile_name = optarg;
      break;
      
    case 'o':
      outFile_name = optarg;
      break;
      
    case 't':
      inHistoTotal_name = optarg;
      break;
      
    case 'p':
      inHistoPassed_name = optarg;
      break;
      
    case 'a':
      adaptive = std::stoul(optarg);
      break;

    case 'r':
      switch_axis = std::stoul(optarg);
      break;
      
    case '1':
      min_events_1 = std::stoi(optarg);
      break;
      
    case '2':
      min_events_2 = std::stoi(optarg);
      break;
      
    case '3':
      min_events_3 = std::stoi(optarg);
      break;
      
    case '4':
      min_events_4 = std::stoi(optarg);
      break;
    
    case '5':
      min_events_5 = std::stoi(optarg);  
      break;
      
    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-i : input file name." << std::endl;
      std::cout << "-o : output file name." << std::endl;
      std::cout << "-t : name of the histo with the total events." << std::endl;
      std::cout << "-p : name of the histo with the passed events." << std::endl;
      std::cout << "-a : 1 or 0, to use the adaptive binning (1) or not (0)." << std::endl;
      std::cout << "-r : 1 or 0, to reverse the x and y axis (1) or not (0)." << std::endl;
      std::cout << "-1 to -5: minimum number of events per bin, for adaptive binning." << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;
      
    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
    }  //switch (ca)                                                                 
  }  //while ((ca = getopt(argc, argv, "i:c:o")) != -1)
  
  //check of the acquired options
  if((inHistoTotal_name == "") || (inHistoPassed_name == "") || (inFile_name == "") || (outFile_name == "")
     || ((adaptive != 0) && (adaptive != 1)) || ((switch_axis != 0) && (switch_axis != 1))){
    std::cout << "Error: input configuration not correctly set." << std::endl;
    exit(EXIT_FAILURE);  
  }
  
  //convert env variables, if needed
  inFile_name = ParseEnvName(inFile_name);
  outFile_name = ParseEnvName(outFile_name);
  
  std::cout << "inFile_name = " << inFile_name << std::endl;
  std::cout << "outFile_name = " << outFile_name << std::endl;
  std::cout << "inHistoTotal_name = " << inHistoTotal_name << std::endl;
  std::cout << "inHistoPassed_name = " << inHistoPassed_name << std::endl;
  std::cout << "adaptive = " << adaptive << std::endl;
  
  //open the output stream for the log, and write the job options
  std::ofstream log_stream(outFile_name + "_log.log", ios::out);
  
  log_stream << "inFile_name = " << inFile_name << std::endl;
  log_stream << "outFile_name = " << outFile_name << std::endl;
  log_stream << "inHistoTotal_name = " << inHistoTotal_name << std::endl;
  log_stream << "inHistoPassed_name = " << inHistoPassed_name << std::endl;
  log_stream << "adaptive = " << adaptive << std::endl;
  
  //open the input file with the histos
  TFile *inFile = TFile::Open(inFile_name.c_str(), "READ");
  
  if(inFile == NULL){
    std::cout << "Input file " << inFile_name << " not found." << std::endl;
    exit(EXIT_FAILURE); 
  }
  
  //open histo with passed events
  TH3D *h_PID_passed = (TH3D*) inFile->Get(inHistoPassed_name.c_str());
  
  //open histo with all events
  TH3D *h_PID_total = (TH3D*) inFile->Get(inHistoTotal_name.c_str());
  
  if((h_PID_passed == NULL) || (h_PID_total == NULL)){
    std::cout << "Error: PID histograms not found!" << std::endl;
    exit(EXIT_FAILURE); 
  }
  else
    std::cout << "PID efficiencies loaded." << std::endl;
  
  //to use the same axis ranges for the error histo, I clone the original histo and then reset its content
  TH3D *h_PID_errors_pass = (TH3D*) h_PID_passed->Clone((outHisto_name + "_errors_pass").c_str());
  h_PID_errors_pass->Reset();
  
  TH3D *h_PID_errors_total = (TH3D*) h_PID_total->Clone((outHisto_name + "_errors_total").c_str());
  h_PID_errors_total->Reset();
  
  //some counters used for debug
  unsigned int num_negative_bins_passed = 0;
  unsigned int num_negative_bins_total = 0;
  
  //I search and fix for negative bins
  FixNegativeBins(h_PID_passed, h_PID_errors_pass, num_negative_bins_passed, false);
  FixNegativeBins(h_PID_total, h_PID_errors_total, num_negative_bins_total, false);
  
  std::cout << "num_negative_bins_passed = " << num_negative_bins_passed
            << " (" << (num_negative_bins_passed/(double) h_PID_passed->GetNcells())*100 << " %)" << std::endl;
  std::cout << "num_negative_bins_total = " << num_negative_bins_total
            << " (" << (num_negative_bins_total/(double) h_PID_total->GetNcells())*100 << " %)" << std::endl;
  
  log_stream << "num_negative_bins_passed = " << num_negative_bins_passed
             << " (" << (num_negative_bins_passed/(double) h_PID_passed->GetNcells())*100 << " %)" << std::endl;
  log_stream << "num_negative_bins_total = " << num_negative_bins_total
             << " (" << (num_negative_bins_total/(double) h_PID_total->GetNcells())*100 << " %)" << std::endl;
  
  //open the output file
  TFile *outFile = TFile::Open((outFile_name + ".root").c_str(), "recreate");

  //now fill TEfficiency or adaptive stuff?
  if(adaptive == 1){
    
    std::cout << "min_events_1 = " << min_events_1 << std::endl;
    std::cout << "min_events_2 = " << min_events_2 << std::endl;
    std::cout << "min_events_3 = " << min_events_3 << std::endl;
    std::cout << "min_events_4 = " << min_events_4 << std::endl;
    std::cout << "min_events_5 = " << min_events_5 << std::endl;
    
    log_stream << "min_events_1 = " << min_events_1 << std::endl;
    log_stream << "min_events_2 = " << min_events_2 << std::endl;
    log_stream << "min_events_3 = " << min_events_3 << std::endl;
    log_stream << "min_events_4 = " << min_events_4 << std::endl;
    log_stream << "min_events_5 = " << min_events_5 << std::endl;
    
    //check that the minimum number of events are fine
    if((min_events_1 < 0) || (min_events_2 < 0) || (min_events_3 < 0)
       || (min_events_4 < 0) || (min_events_5 < 0)){
      std::cout << "Error: the minimum number of events per bin are not correctly set." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    makeAdaptive(h_PID_passed, h_PID_total, outFile, outHisto_name, log_stream, switch_axis,
                 std::vector<int>{min_events_1, min_events_2, min_events_3, min_events_4, min_events_5});
  }
  else
    makeTEfficiency(h_PID_passed, h_PID_total, h_PID_errors_pass, h_PID_errors_total, outFile, outHisto_name, switch_axis);

  //write the simple average efficiency in the log stream
  log_stream << "simple mean efficiency = " << ((double) h_PID_passed->GetEntries())/ ((double) h_PID_total->GetEntries())
             << " +- " << BinomialError(h_PID_passed->GetEntries(), h_PID_total->GetEntries()) << std::endl;
  
  //write some debug histos
  outFile->cd();
  h_PID_errors_pass->Write(h_PID_errors_pass->GetName());
  h_PID_errors_total->Write(h_PID_errors_total->GetName());
  
  //close the files
  inFile->Close();
  outFile->Close();
  
  //close and write the output log
  log_stream.close();
  
  return 0;

}
