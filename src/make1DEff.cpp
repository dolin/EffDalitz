/*!
 *  @file      mean1DEff.cpp
 *  @author    Alessio Piucci
 *  @date      27-03-2018
 *  @brief     Macro to compute the efficiency over 1D distributions
 *  @return    Create an output .root file with the efficiency distributions
 */

//from std
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//from ROOT
#include <TROOT.h>
#include "TFile.h"
#include "TTree.h"
#include <TEfficiency.h>

#include "commonLib.h"
#include <../IOjuggler/IOjuggler.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

using namespace std;
namespace pt = boost::property_tree;

int main(int argc, char** argv){

  //-------------------------//
  //  parse the job options  //
  //-------------------------//
  
  std::string inFile_num_name = "";
  std::string inTree_num_name = "";
  
  std::string inFile_den_name = "";
  std::string inTree_den_name = "";

  std::string configFile_name = "";
  
  std::string outFile_name = "";
  
  extern char* optarg;
  
  int ca;
  
  //parsing the input options
  while ((ca = getopt(argc, argv, "n:l:d:m:c:o:h")) != -1){
    
    switch (ca){
      
    case 'n':
      inFile_num_name = optarg;
      break;

    case 'l':
      inTree_num_name = optarg;
      break;
      
    case 'd':
      inFile_den_name = optarg;
      break;

    case 'm':
      inTree_den_name = optarg;
      break;
      
    case 'c':
      configFile_name = optarg;
      break;
      
    case 'o':
      outFile_name = optarg;
      break;
      
    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-i : input file name containing numerator tree" << std::endl;
      std::cout << "-l : name of the numerator tree" << std::endl;
      std::cout << "-d : input file name containing denominator tree" << std::endl;
      std::cout << "-m : name of the denominator tree" << std::endl;
      std::cout << "-c : config file name" << std::endl;
      std::cout << "-o : output file name" << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;
      
    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
    }  //switch (ca)                                                                 
  }  //while ((ca = getopt(argc, argv, "i:c:o")) != -1)
  
  //convert env variables, if needed
  inFile_num_name = ParseEnvName(inFile_num_name);
  inFile_den_name = ParseEnvName(inFile_den_name);
  configFile_name = ParseEnvName(configFile_name);
  outFile_name = ParseEnvName(outFile_name);
  
  std::cout << "inFile_num_name = " << inFile_num_name << std::endl;
  std::cout << "inTree_num_name = " << inTree_num_name << std::endl;
  std::cout << "inFile_den_name = " << inFile_den_name << std::endl;
  std::cout << "inTree_den_name = " << inTree_den_name << std::endl;
  std::cout << "configFile_name = " << configFile_name << std::endl;
  std::cout << "outFile_name = " << outFile_name << std::endl;
  
  //check that everything is fine
  if((inFile_num_name == "") || (inTree_num_name == "")
     || (inFile_den_name == "") || (inTree_den_name == "")
     || (configFile_name == "") || (outFile_name == "")){
    std::cout << "Error: some of the parsed options are not valid." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //configuration Boost tree
  pt::ptree configtree;
  
  //parse the INFO into the property tree
  pt::read_info(configFile_name, configtree);
  auto_append_in_ptree(configtree);
  
  //open the input file of the numerator
  TFile* inFile_num = TFile::Open(inFile_num_name.c_str(), "READ");
  
  //open the numerator tree
  TTree* inTree_num = (TTree*) inFile_num->Get(inTree_num_name.c_str());
  
  if(inTree_num == nullptr){
    std::cout << "Error: tree_num does not exist." << std::endl;
    exit(EXIT_FAILURE); 
  }

  //open the input file of the denominator
  TFile* inFile_den = TFile::Open(inFile_den_name.c_str(), "READ");
  
  //open the denominator tree
  TTree* inTree_den = (TTree*) inFile_den->Get(inTree_den_name.c_str());
  
  if(inTree_den == nullptr){
    std::cout << "Error: tree_den does not exist." << std::endl;
    exit(EXIT_FAILURE); 
  }
  
  //define the numerator and denominator histos
  TH1D *h_num = new TH1D("h_num", "",
			 configtree.get<unsigned int>("binning"),  //binning
			 configtree.get<double>("xaxis_min"),
			 configtree.get<double>("xaxis_max"));
  
  TH1D *h_den = new TH1D("h_den", "",
                         configtree.get<unsigned int>("binning"),  //binning
                         configtree.get<double>("xaxis_min"),
                         configtree.get<double>("xaxis_max"));

  h_num->Sumw2();
  h_den->Sumw2();
  
  //get the cuts to apply to the numerator and denominator
  std::string cuts_num = configtree.get<std::string>("num_cuts");
  std::string cuts_den = configtree.get<std::string>("den_cuts");

  std::cout << "cuts_num = " << cuts_num << std::endl;
  std::cout << "cuts_den = " << cuts_den << std::endl;
  
  //finally fill the numerator and denominator histos
  inTree_num->Draw((configtree.get<std::string>("num_variable_name") + ">>h_num").c_str(),
		   cuts_num.c_str(), "");
  inTree_den->Draw((configtree.get<std::string>("den_variable_name") + ">>h_den").c_str(),
		   cuts_den.c_str(), "");

  //some style
  h_num->GetXaxis()->SetTitle((configtree.get<std::string>("xaxis_label")).c_str());
  h_num->GetYaxis()->SetTitle("entries");

  h_den->GetXaxis()->SetTitle((configtree.get<std::string>("xaxis_label")).c_str());
  h_den->GetYaxis()->SetTitle("entries");
  
  //define the eff histo
  TEfficiency *h_eff = new TEfficiency(*h_num, *h_den);
  
  //save the histos in the output file
  TFile *outFile = new TFile(outFile_name.c_str(), "recreate");
  
  outFile->cd();
  
  h_num->Write((configtree.get<std::string>("numHisto_name")).c_str());
  h_den->Write((configtree.get<std::string>("denHisto_name")).c_str());
  h_eff->Write((configtree.get<std::string>("effHisto_name")).c_str());
    
  outFile->Write();

  std::cout << std::endl;
  std::cout << "Saved the histos as:" << std::endl;
  std::cout << "-- numerator:   " << configtree.get<std::string>("numHisto_name") << std::endl;
  std::cout << "-- denominator: " << configtree.get<std::string>("denHisto_name") << std::endl;
  std::cout << "-- efficiency:  " << configtree.get<std::string>("effHisto_name") << std::endl;
  std::cout << std::endl;
  
  //close all the files
  inFile_num->Close();
  inFile_den->Close();
  outFile->Close();
    
  return 0;
}
