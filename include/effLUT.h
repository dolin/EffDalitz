#ifndef INCLUDE_EFFLUT_H 
#define INCLUDE_EFFLUT_H 1

/** @class effLUT effLUT.h include/effLUT.h               
 *                                                  
 *                                                                   
 *  @author   Alessio Piucci                                              
 *  @date     15-03-2016
 *  @brief    Script to retrieve the efficiency value for a given candidate, reading from lookup tables.             
 *  @return   Returns the efficiency value.                                                               
 */

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <utility>   //to use std::pair
#include <tuple>
#include <fstream>   //to write the log in an external stream

#include "commonLib.h"
#include "../2DAdaptiveBinning/include/TH2A.h"

//ROOT libraries
#include "TString.h"
#include "TH2D.h"
#include "TLorentzVector.h"
#include "TFile.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TVectorD.h"
#include "TKey.h"
#include "TObjString.h"

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>

using namespace std;
namespace pt = boost::property_tree;

struct Efficiency{
  
  //efficiency value
  double efficiency;
  
  //efficiency errors
  double eff_err;
  double eff_err_low;
  double eff_err_up;
  
  //total and passed number of events
  double num_total;
  double num_passed;
};


class effLUT {

public: 
  
  /// Standard constructor
  effLUT(const std::string _configFileName, TFile* _outFile); 

  ~effLUT( ); ///< Destructor
  
  // get efficiency as function of full kinematics of the event
  void GetEventEff(const double Kin_1, const double Kin_2,
		   const std::map<std::string, std::pair<double, double>> BDT_vars,
                   const std::map<std::string, TLorentzVector> PIDqvects,
                   const unsigned int nTracks, const bool varxP_varyEta,
                   Efficiency &FiducialEff, Efficiency &GeneratorEff, Efficiency &TriggerToSelEff,
                   Efficiency &KinEff_tot, Efficiency &BDTEff_tot, Efficiency &PIDEff_tot,
                   Efficiency &comb_eff, const std::string &year);
  
  // get efficiency of kinematic selection from a Dalitz plot
  // with part1-part2 and part2-part3 invariant masses
  void GetKineEff(const double Kin_1, const double Kin_2,
                  Efficiency &FiducialEff, Efficiency &GeneratorEff, Efficiency &TriggerToSelEff,
                  Efficiency &KinEff_tot, const std::string &year);
  
  // get the BDT efficiency for two cuts
  void GetBDTEff(const std::map<std::string, std::pair<double, double>> BDT_vars,
                 Efficiency &BDTEff_tot, const std::string &year);
  
  // get the efficiency for the combined PID cuts
  void GetPIDEff(const std::map<std::string, TLorentzVector> PIDqvects,
                 const double nTracks, const bool varxP_varyEta,
                 Efficiency &PIDEff, const std::string &year);
  
  // get a single efficiency from a TH2A object, with total of total and passed events
  void GetSingleEff(const double var_1, const double var_2,
                    TH2A *h_eff, Efficiency &Eff);
    
  // get the single components of the kinematic efficiency
  bool GetKinComponents(const double Kin_1, const double Kin_2,
                        Efficiency &FiducialEff, Efficiency &GeneratorEff,
                        Efficiency &TriggerToSelEff,
                        Efficiency &TriggerEff, Efficiency &RecoEff,
                        Efficiency &SelectionEff, const std::string &year);
    
  // to load the fiducial efficiency
  Efficiency LoadFiducialEff(std::string fiducial_config);
  
  // to load TH2A maps
  void LoadMaps();

  // set the kinematic efficiencies 
  void SetKinEfficiencies();
    
  // set the lookup table for the kinematic efficiency
  TH2A* SetKinLookuptable(TFile *inEffFile,
                          std::string effFile_config, std::string effHisto_config);
  
  // set the PID efficiencies
  void SetPIDEfficiencies();

  // set the BDT efficiencies
  void SetBDTEfficiencies();

  // pair of particle names and 4-momenta for BDT and PID cuts
  std::string GetParticleNameBDT(){return configtree.get<std::string>("BDTEff.name_part");};
  std::string GetParticleNamePID(){return configtree.get<std::string>("PIDEff.name_part");};

  // to combine single efficiencies
  void CombineEfficiencies(Efficiency &TotEff, const std::vector<Efficiency> Eff_vect);
  
  // set dummy values for the efficiency
  void SetUnvalidEfficiency(Efficiency &Eff){
    Eff.efficiency = -1.;
    Eff.eff_err = -1.;
    Eff.eff_err_low = -1.;
    Eff.eff_err_up = -1.;
    Eff.num_total = -1.;
    Eff.num_passed = -1.;
  };

  // set dummy values for the efficiency, = 1.
  void SetEfficiencyOne(Efficiency &Eff){
    Eff.efficiency = 1.;
    Eff.eff_err = 0.;
    Eff.eff_err_low = 0.;
    Eff.eff_err_up = 0.;
    Eff.num_total = 1.;
    Eff.num_passed = 1.; 
  };
  
  // set dummy values for the efficiency, = 0.
  void SetNullEfficiency(Efficiency &Eff){
    Eff.efficiency = 0.;
    Eff.eff_err = 0.;
    Eff.eff_err_low = 0.;
    Eff.eff_err_up = 0.;
    Eff.num_total = 0.;
    Eff.num_passed = 0.; 
  };
  
  // to check if an efficiency is valid
  bool CheckValidEfficiency(const Efficiency Eff){
    return !(std::isnan(Eff.efficiency) || (Eff.efficiency <= 0.) || (Eff.efficiency > 1.000001)
             || std::isnan(Eff.eff_err) || (Eff.eff_err < 0.) || (Eff.eff_err > 1.000001));
  };
  
protected:
  
private:
  
  //to flag which efficiencies are enabled
  bool SingleKinComponents_eff_on;
  bool Fiducial_Eff_on;
  bool Generator_Eff_on;
  bool TriggerToSel_Eff_on;
  
  bool kin_eff_on;
  bool BDT_eff_on;
  bool PID_eff_on;
  
  //years to take into account
  std::vector<std::string> years;
  
  //fiducial efficiency
  std::map<std::string, Efficiency> Fiducial_eff_map;
  
  //generator efficiency
  std::map<std::string, TFile*> GeneratorEffFile_map;
  std::map<std::string, TH2A*> h_generator_eff_adaptive_map;

  //trigger->sel efficiency
  std::map<std::string, TFile*> TriggerToSelEffFile_map;
  std::map<std::string, TH2A*> h_TriggerToSel_eff_adaptive_map;
  
  //variables for the single components of the kinematic efficiency (trigger->selection)
  std::map<std::string, TFile*> TriggerEffFile_map;
  std::map<std::string, TFile*> RecoEffFile_map;
  std::map<std::string, TFile*> SelectionEffFile_map;
  
  std::map<std::string, TH2A*> h_trigger_eff_adaptive_map;
  std::map<std::string, TH2A*> h_reco_eff_adaptive_map;
  std::map<std::string, TH2A*> h_selection_eff_adaptive_map;
  
  //mega-structure for the BDT cuts
  //I have a group of different PID cuts, each of it referring to a particle
  //and then I have different years for each of the particle
  //<key: particle name, <key: year, eff map>>
  std::map<std::string, std::map<std::string, TH2A*>> BDT_eff_adaptive_inception;
  
  //mega-structure for the PID cuts
  //I have a group of different PID cuts, each of it referring to a particle
  //and then I have different years for each of the particle
  //In addition, for each of the <particle, year> pair,
  //I have different efficiency histograms, each of them referring to a (nTracks_min, nTracks_max) interval
  //
  //At the end, I use a map of maps of vector of ntuples (!!):
  //<key: particle name, <key: year, array of <z_min, z_max, eff map>>>
  std::map<std::string, std::map<std::string, std::vector<std::tuple<double,double,TH2A*>>>> PID_eff_adaptive_inception;
  
  //config file
  std::string configFileName;
  pt::ptree configtree;

  //output file
  TFile* outFile;
  
  //variables used for combining efficiency
  double alpha;
  double beta;
  double conf_level;
  
  //error log
  std::ofstream log_errors_stream;
  
};

#endif // INCLUDE_EFFLUT_H
